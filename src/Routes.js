import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import { Icon } from 'native-base';
import { TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Platform } from 'react-native';
import { connect } from 'react-redux';

import LoginScreen from './screens/login/LoginScreen';
import RecoveryPassScreen from './screens/login/RecoveryPassScreen';
import UserDataScreen from './screens/client';
import Inspencao from './screens/inspencao/InspecaoScreen';
import Services from './screens/services/Services';
import Petrol from './screens/petrol/Petrol';
import EditPetrol from './screens/petrol/EditPetrol';
import Tanks from './screens/tanks/Tanks';
import EditTanks from './screens/tanks/EditTanks';
import Filters from './screens/filters/Filters';
import PasswordChange from './screens/password/PasswordChange';
import FirstInspection from './screens/inspencao/FirstInspection';
import SecondInspection from './screens/inspencao/SecondInspection';
import ThirdInspection from './screens/inspencao/ThirdInspection';
import { colors } from './utils';
import { handleLogin } from './screens/login/store/actions';
import { getInspectionsAction } from './screens/inspencao/store/actions';
import { getUserAcess } from './utils/localStore';

class Routes extends Component {

    async componentWillMount() {
        const user = await getUserAcess();
        if (user !== null) {
          this.props.handleLogin(user.email, user.password);
          this.props.getInspectionsAction();
          Actions.inspencao();
        }
      };
      
    menuLeft = () => (
        <TouchableOpacity style={{ marginLeft: 10 }} onPress={this.props.openMenu}>
            <Icon ios='ios-menu' android="md-menu" style={{ fontSize: 24, color: colors.black }} />
        </TouchableOpacity>
    );

    _backLogin = () => (
        <Icon name="ios-arrow-back" size={30} style={{ color: '#fff', marginLeft: 10 }} onPress={() => { Actions.pop(); }} />
    );

    render() {
        return (
            <Router navigationBarStyle={{ backgroundColor: colors.blueMain }} titleStyle={{ color: colors.black, fontWeight: '400', fontSize: 20 }}>
                <Scene key='root'>
                    <Scene hideNavBar key='login' >
                        <Scene key='login' component={LoginScreen} type='replace' />
                        <Scene key='recovery' component={RecoveryPassScreen} hideNavBar={false} navTransparent left={this._backLogin} />
                    </Scene>
                    <Scene hideNavBar>
                        <Scene key='inspencao' component={Inspencao} titleStyle={Platform.OS === 'ios' ? styles.tituloIos : styles.titulo} hideNavBar={false} left={this.menuLeft} />
                        <Scene key='user_data_screen' component={UserDataScreen} titleStyle={Platform.OS === 'ios' ? styles.tituloIos : styles.titulo} hideNavBar={false} left={this.menuLeft} />
                        <Scene key='first_inspection' component={FirstInspection} titleStyle={Platform.OS === 'ios' ? styles.tituloIos : styles.titulo} hideNavBar={false} left={this.menuLeft} />
                        <Scene key='second_inspection' component={SecondInspection} titleStyle={Platform.OS === 'ios' ? styles.tituloIos : styles.titulo} hideNavBar={false} left={this.menuLeft} />
                        <Scene key='third_inspection' component={ThirdInspection} titleStyle={Platform.OS === 'ios' ? styles.tituloIos : styles.titulo} hideNavBar={false} left={this.menuLeft} />
                        <Scene key='services' component={Services} titleStyle={Platform.OS === 'ios' ? styles.tituloIos : styles.titulo} hideNavBar={false} left={this.menuLeft} />
                        <Scene key='petrol' component={Petrol} titleStyle={Platform.OS === 'ios' ? styles.tituloIos : styles.titulo} hideNavBar={false} left={this.menuLeft} />
                        <Scene key='edit_petrol' component={EditPetrol} titleStyle={Platform.OS === 'ios' ? styles.tituloIos : styles.titulo} hideNavBar={false} left={this.menuLeft} />
                        <Scene key='tanks' component={Tanks} titleStyle={Platform.OS === 'ios' ? styles.tituloIos : styles.titulo} hideNavBar={false} left={this.menuLeft} />
                        <Scene key='edit_tanks' component={EditTanks} titleStyle={Platform.OS === 'ios' ? styles.tituloIos : styles.titulo} hideNavBar={false} left={this.menuLeft} />
                        <Scene key='filters' component={Filters} titleStyle={Platform.OS === 'ios' ? styles.tituloIos : styles.titulo} hideNavBar={false} left={this.menuLeft} />
                        <Scene key='change_pass' component={PasswordChange} titleStyle={Platform.OS === 'ios' ? styles.tituloIos : styles.titulo} hideNavBar={false} left={this.menuLeft} />
                    </Scene>
                </Scene>
            </Router>
        );
    }
};

const styles = {
    titulo: {
        flex: 1,
        marginRight: '25%',
        textAlign: 'center',
        fontSize: 24,
        fontWeight: 'normal',
    },
    tituloIos: {
        fontSize: 24,
        fontWeight: 'normal',
    }
};

export default connect(null, { handleLogin, getInspectionsAction })(Routes); 

