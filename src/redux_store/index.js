import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

// import dos reducers da aplicação

import loginReducer from '../screens/login/store/reducer';
import clientReducer from '../screens/client/store/reducer';
import inspectionReducer from '../screens/inspencao/store/reducer';
import petrolReducer from '../screens/petrol/store/reducer';
import tanksReducer from '../screens/tanks/store/reducer';


const reducers = combineReducers({
    auth: loginReducer,
    client: clientReducer,
    inspection: inspectionReducer,
    tankList: tanksReducer,
    petrolList: petrolReducer,
});

export default function configureStore(initialState = {}) {
    const middlewares = [thunk];
    const store = createStore(
        reducers,
        initialState,
        applyMiddleware(...middlewares)
    );
    return store;
}