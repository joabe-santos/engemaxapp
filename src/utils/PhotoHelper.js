import React from 'react';
import ImageResizer from 'react-native-image-resizer';
import ImagePicker from 'react-native-image-picker'

const placeHolder = require('../assets/icone.png');

export const imageValidate = (img) => {
  if (img !== null) {
    return { uri: img };
  }
  return placeHolder;
};

export function PhotoHelper(setImage) {
  const defaultOptions = {
    title: 'Escolha a Foto',
    storageOptions: {
      skipBackup: true,
      path: 'images',
      waitUntilSaved: true
    },
    noData: false,
    chooseFromLibraryButtonTitle: 'Escolha da sua Biblioteca',
    takePhotoButtonTitle: 'Tirar foto',
    cancelButtonTitle: 'Voltar'
  };

  const options = { ...defaultOptions };

  ImagePicker.launchCamera(options, (response) => {
    if (response.error) {
      console.log('erro imagepicker');
    } else if (response.didCancel) {
      console.log('User cancelled image picker');
    } else {
      let rotation = 0
      if (response.originalRotation === 90) {
        rotation = 90
      } else if (response.originalRotation === 270) {
        rotation = -90
      }
      ImageResizer.createResizedImage(response.uri, 400, 400, 'JPEG', 100, rotation, null).then((response_img) => {
        setImage(response_img.uri);
      }).catch((err) => {
        console.log('Problem convert img::', err);
      });
    }
  });
}

