import { AsyncStorage } from 'react-native';

export function setUserAcess(data) {
    AsyncStorage.setItem('@useracess', JSON.stringify(data), () => {
        AsyncStorage.mergeItem('@useracess', JSON.stringify(data), () => {

        });
    });
};

export async function getUserAcess() {
    const user = await AsyncStorage.getItem('@useracess');
    return JSON.parse(user);
};
  
export function clearUserAcess() {
    AsyncStorage.removeItem('@useracess');
};

//Dados do Usuário

export function setUser(data) {
    AsyncStorage.setItem('@user', JSON.stringify(data), () => {
        AsyncStorage.mergeItem('@user', JSON.stringify(data), () => {

        });
    });
};

export async function getUser() {
    const user = await AsyncStorage.getItem('@user');
    return JSON.parse(user);
};
  
export function clearUser() {
    AsyncStorage.removeItem('@user');
};