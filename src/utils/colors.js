const colors = {
    blueMain: '#0C94CE',
    white: '#FFFFFF',
    black: '#000000',
    green: '#54893B',
    gray: '#9B9B9B',
    grayFont: '#4A4A4A',
    line:'#E6E4E4'
}
 export default colors;
 