const bombas = require('./bombas.png');
const filter = require('./filter.png');
const inspecao = require('./inspecao.png');
const iosmenu = require('./ios-menu.png');
const key = require('./key.png');
const logout = require('./logout.png');
const service = require('./service.png');
const tank = require('./tank.png');
const add_button = require('./add-button.png');

const icons = {
    bombas,
    filter,
    inspecao,
    iosmenu,
    key,
    logout,
    service,
    tank,
    add_button
};

export default icons;
