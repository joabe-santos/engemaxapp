import React, { Component } from 'react';
import { Provider } from 'react-redux';

import DrawerComponent from './components/drawer/DrawerComponent';
import configureStore from './redux_store/index';
const store = configureStore();

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <DrawerComponent />
      </Provider>
    );
  }
}

