import React from 'react';
import { Form, Text } from 'native-base';
import { colors } from '../../utils';
import { RadioButton } from '../forms';

export const ListCombustivel = ({ data, title, click, selected }) => (
  <Form style={styles.container}>
    <Text style={styles.txt}>{title}</Text>
    <Form style={styles.containerRadio}>
      {data.map((itens, i) => (
        <RadioButton
          key={i}
          label={itens.label}
          press={() => click(itens)}
          value={selected !== null && selected.value === itens.value}
        />
      )
      )}
    </Form>
  </Form>
)

export const ListConveniencia = ({ data, title, click, selected }) => (
  <Form style={styles.container}>
    <Text style={styles.txt}>{title}</Text>
    <Form style={styles.containerRadio}>
      {data.map((itens, i) => (
        <RadioButton
          key={i}
          label={itens.label}
          press={() => click(itens)}
          value={selected !== null && selected.value === itens.value}
        />
      )
      )}
    </Form>
  </Form>
)

export const ListTrocaOleo = ({ data, title, click, selected }) => (
  <Form style={styles.container}>
    <Text style={styles.txt}>{title}</Text>
    <Form style={styles.containerRadio}>
      {data.map((itens, i) => (
        <RadioButton
          key={i}
          label={itens.label}
          press={() => click(itens)}
          value={selected !== null && selected.value === itens.value}
        />
      )
      )}
    </Form>
  </Form>
)

export const ListVeiculos = ({ data, title, click, selected }) => (
  <Form style={styles.container}>
    <Text style={styles.txt}>{title}</Text>
    <Form style={styles.containerRadio}>
      {data.map((itens, i) => (
        <RadioButton
          key={i}
          label={itens.label}
          press={() => click(itens)}
          value={selected !== null && selected.value === itens.value}
        />
      )
      )}
    </Form>
  </Form>
)

export const ListTercerizada = ({ data, title, click, selected, edit }) => (
  <Form style={styles.container}>
    <Text style={styles.txt}>{title}</Text>
    <Form style={styles.containerTerceira}>
      {data.map((itens, i) => (
        <RadioButton
          key={i}
          label={itens.label}
          press={() => click(itens)}
          value={selected !== null && selected.value === itens.value}
          edit={edit}
        />
      )
      )}
    </Form>
  </Form>
)

const styles = {
  container: {
    borderBottomWidth: 1,
    borderBottomColor:
      colors.gray, margin: 5
  },
  containerRadio: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5, width: '70%'
  },
  containerTerceira: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 5, width: '86%'
  },
  txt: {
    fontSize: 15,
    marginLeft: 7,
    color: colors.gray
  },
}

