import React from 'react';
import { Text } from 'native-base';
import { colors } from '../../utils';
import { RadioButton } from '../../components/forms';
import { View } from 'react-native';

export const ItensBombas = ({ data, title, selected, click, bombaNunber }) => (
    <View style={styles.containerBomba}>
        <Text style={styles.txtBomba}>Bomba 0{bombaNunber + 1}</Text>
        <Text style={styles.txt}>{title}</Text>
        <View style={[styles.bombaContainer, styles.space]}>
            {data.map((itens, i) => (
                <RadioButton
                    key={i}
                    label={itens.label}
                    press={() => click(itens)}
                    value={selected !== null && selected.value === itens.value}
                />
            )
            )
            }
        </View>
    </View>
)

handle = (click, it, value, type) => {
    const index = value.findIndex(val => val.value === it.value);
    if (type.value === 'SIMPLES') {
        click([it])
    }
    if (type.value === 'DUPLA') {        
        if (index > -1) {
            click(value.filter(item => item.value !== it.value));
        } else {
            if (value.length <= 1) {
                click([...value, it])
            }
        }
    }
    if (type.value === 'TRIPLA') {
        if (index > -1) {
            click(value.filter(item => item.value !== it.value));
        } else {
            if (value.length <= 2) {
                click([...value, it])
            }
        }
    }
}

checkSelected = (selected, item) => {
    const ix = selected.findIndex(val => val.value === item.value);
    return ix;
}

export const ItensCombustiveis = ({ data, title, selected, click, type }) => (
    <View style={styles.containerComubustivel}>
        <Text style={styles.txt}>{title}</Text>
        <View style={styles.itemContainer}>
            {data.map((itens, i) => (
                <View style={{ width: '50%' }} key={i}>
                    <RadioButton
                        key={i}
                        label={itens.label}
                        press={() => handle(click, itens, selected, type)}
                        value={checkSelected(selected, itens) > -1}
                    />
                </View>
            )
            )
            }
        </View>
    </View>
)

const styles = {
    containerBomba: {
        borderBottomWidth: 1,
        borderColor: colors.line
    },
    txtBomba: {
        color: colors.blueMain
    },
    bombaContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '80%',
        marginBottom: 5
    },
    space: {
        width: '80%'
    },
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
    },
    containerComubustivel: {
        flex: 1,
        paddingTop: 10,
        borderBottomWidth: 1,
        borderColor: colors.line,
        paddingBottom: 5
    },
    txt: {
        fontSize: 15,
        marginLeft: 7,
        color: colors.gray
    },
};
