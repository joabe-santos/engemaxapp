import React from 'react';
import { Text } from 'native-base';
import { colors } from '../../utils';
import { RadioButton } from '../../components/forms';
import { View } from 'react-native';

export const ItensTanks = ({ data, title, click, selected, tanqueNumber }) => (
    <View style={styles.containerTank}>
        <Text style={styles.txtTank}>Tanque 0{tanqueNumber + 1}</Text>
        <Text style={styles.txt}>{title}</Text>
        <View style={styles.TanksContainer}>
            {data.map((itens, i) => (
                <RadioButton
                    key={i}
                    label={itens.label}
                    press={() => click(itens)}
                    value={selected !== null && selected.value === itens.value}
                />
            )
            )
            }
        </View>
    </View>
)

export const ItensTanksVolume = ({ data, title, click, selected }) => (
    <View style={styles.containerTank}>
        <Text style={styles.txt}>{title}</Text>
        <View style={styles.TanksContainer}>
            {data.map((itens, i) => (
                <RadioButton
                    key={i}
                    label={itens.label}
                    press={() => click(itens)}
                    value={selected !== null && selected.value === itens.value}
                />
            )
            )
            }
        </View>
    </View>
)

    handleSelect = (click, it, value, type) => {
        const index = value.findIndex(val => val.value === it.value);
        if (type.value === 'PLENO') {
            click([it])
        }
        if (type.value === 'BIPARTIDO') {
            if (index > -1) {
                click(value.filter(item => item.value !== it.value));
            } else {
                if(value.length <= 1) {
                    click([...value, it]);
                } 
            }
        }
        if (type.value === 'TRIPARTIDO') {
            if (index > -1) {
                click(value.filter(item => item.value !== it.value));
            } else {
                if(value.length <= 2) {
                    click([...value, it]);
                } 
            }
        }
    }

    checkSelected = (selected, item) => {
        const ix = selected.findIndex(val => val.value === item.value);
        return ix;
    }

export const ItensTanksCombustiveis = ({ data, title, click, selected, type }) => (
    <View style={styles.containerComubustivel}>
        <Text style={styles.txt}>{title}</Text>
        <View style={styles.itemContainer}>
            {data.map((itens, i) => (
                <View style={{ width: '50%' }} key={i}>
                    <RadioButton
                        key={i}
                        label={itens.label}
                        press={() => handleSelect(click, itens, selected, type)}
                        value={checkSelected(selected, itens) > -1}
                    />
                </View>
            )
            )
            }
        </View>
    </View>
)

const styles = {
    containerTank: {
        borderBottomWidth: 1,
        borderColor: colors.line
    },
    txtTank: {
        color: colors.blueMain
    },
    TanksContainer: {
        flexDirection: 'row',
        marginBottom: 5,
        justifyContent: 'space-between',

    },
    itemContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
    },
    containerComubustivel: {
        flex: 1,
        paddingTop: 10,
        borderBottomWidth: 1,
        borderColor: colors.line,
        paddingBottom: 5
    },
    txt: {
        fontSize: 15,
        marginLeft: 7,
        color: colors.gray
    },
};
