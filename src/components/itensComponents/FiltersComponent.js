import React from 'react';
import { Form, Text } from 'native-base';
import { colors } from '../../utils';
import { RadioButton } from '../../components/forms';

export const ListDiesel = ({ data, title, selected, click, edit }) => (
    <Form style={styles.container}>
        <Text style={styles.txt}>{title}</Text>
        <Form style={[styles.containerRadio, styles.space]}>
            {data.map((itens, i) => (
                <RadioButton
                    key={i}
                    label={itens.label}
                    press={() => click(itens)}
                    value={selected !== null && selected.value === itens.value}
                    edit={edit}
                />
            )
            )}
        </Form>
    </Form>
)


export const ListFiltros = ({ data, title, selected, click, edit }) => (

    <Form style={styles.container}>
        <Text style={styles.txt}>{title}</Text>
        <Form style={styles.containerRadio}>
            {data.map((itens, i) => (
                <RadioButton
                    key={i}
                    label={itens.label}
                    press={() => click(itens)}
                    value={selected !== null && selected.value === itens.value}
                    edit={edit}
                />
            )
            )
            }
        </Form>
    </Form>
)

export const ListSeparadoras = ({ data, title, selected, click, edit }) => (

    <Form style={styles.container}>
        <Text style={styles.txt}>{title}</Text>
        <Form style={styles.containerRadio}>
            {data.map((itens, i) => (
                <RadioButton
                    key={i}
                    label={itens.label}
                    press={() => click(itens)}
                    value={selected !== null && selected.value === itens.value}
                    edit={edit}
                />
            )
            )
            }
        </Form>
    </Form>
)

export const ListBox = ({ data, title, selected, click }) => (
    <Form style={styles.container}>
        <Text style={styles.txt}>{title}</Text>
        <Form style={[styles.containerRadio, styles.space]}>
            {data.map((itens, i) => (
                <RadioButton
                    key={i}
                    label={itens.label}
                    press={() => click(itens)}
                    value={selected !== null && selected.value === itens.value}
                />
            )
            )
            }
        </Form>
    </Form>
)

const styles = {
    container: {
        borderBottomWidth: 1,
        borderBottomColor:
            colors.gray, margin: 5
    },
    containerRadio: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 5,

    },
    txt: {
        fontSize: 15,
        marginLeft: 7,
        color: colors.gray
    },
    space:{
        width: '70%'
    },
}