import React from 'react';
import { Form, Text } from 'native-base';
import { TouchableOpacity } from 'react-native';
import { colors } from '../../utils';

const meses = {
    '01': 'JANEIRO',
    '02': 'FEVEREIRO',
    '03': 'MARÇO',
    '04': 'ABRIL',
    '05': 'MAIO',
    '06': 'JUNHO',
    '07': 'JULHO',
    '08': 'AGOSTO',
    '09': 'SETEMBRO',
    '10': 'OUTUBRO',
    '11': 'NOVEMBRO',
    '12': 'DEZEMBRO'
};
 
const setDataCadas = (item) => {
    let data = '';
    item.map((it) => {
       data = it.data_cad;
    })
    return data;
};

const InspectionComponent = ({ item, goDetails }) => (
    <Form>  
        <Form style={styles.container}>
            <Text style={styles.txtHeader}>{meses[setDataCadas(item).slice(5, 7)]}, {setDataCadas(item).slice(0,4)}</Text>
        </Form>
        {item.map((itens, i) => (
        <TouchableOpacity key={i} onPress={() => goDetails(itens.hashcod)}>
            <Form>
                <Text style={styles.txtColor}>{itens.nome}</Text>
                <Text style={styles.txt}>{itens.nome_responsavel}</Text>
                <Form style={styles.form}>
                    <Text style={styles.txt}>{itens.data_cad.slice(8, 10)}/{itens.data_cad.slice(5, 7)}/{itens.data_cad.slice(0, 4)}</Text>
                    <Text style={styles.txt}>{itens.hora_cad.slice(0, 5)}</Text>
                </Form>
                <Form style={styles.borderLine}></Form>
            </Form>
        </TouchableOpacity>
        ))}
    </Form>
);

const styles = {
    container: {
        backgroundColor: colors.blueMain,
        marginBottom: 10,
        height: 45,
        justifyContent: 'center'
    },
    txtHeader: {
        fontSize: 20,
        marginLeft: 14,
        color: colors.white
    },
    txtColor: {
        marginLeft: 7,
        color: colors.blueMain,
        fontSize: 20
    },
    txt: {
        fontSize: 15,
        marginLeft: 7,
        color: colors.gray
    },
    borderLine: {
        flex: 1,
        height: 1,
        borderColor: colors.line,
        borderWidth: 0.5
    },
    form: {
        flexDirection: 'row'
    }
}

export default InspectionComponent;
