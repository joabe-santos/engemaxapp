import React from 'react';
import { Form, Label } from 'native-base';
import { TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { colors } from '../../utils';


const navigation = (forward, go) => {
    console.log('teste :::::', forward);
    switch(go) {
        case 1:
            Actions.first_inspection();
        break;
        case 2:
        if (forward) {
            Actions.second_inspection();
        }
        break;
        case 3:
        if (forward) {
            Actions.third_inspection();
        }
        break;
            default: 1
    }
}

const CountPage = ({ act1, act2, act3, forward }) => (
    <Form style={styles.wrpper}>
        <TouchableOpacity onPress={() => navigation(forward, 1)}>
            <Form style={[styles.body, act1]}>
                <Label style={styles.txt}> 1 </Label>
            </Form>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation(forward, 2)}>
            <Form style={[styles.body, act2]}>
                <Label style={styles.txt}> 2 </Label>
            </Form>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation(forward, 3)}>
            <Form style={[styles.body, act3]}>
                <Label style={styles.txt}> 3 </Label>
            </Form>
        </TouchableOpacity>
    </Form>
)

const styles = {
    wrpper: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10
    },
    body: {
        height: 30,
        width: 30,
        borderRadius: 15,
        backgroundColor: colors.blueMain,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10
    },
    txt: {
        color: colors.white
    }
}

export default CountPage;
