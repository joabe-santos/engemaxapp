import React from 'react';
import { Label, Form, Icon } from 'native-base';
import { TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { colors } from '../../utils';

const NavbarAux = ({ title }) => (
    <Form style={styles.line}>
        <Form style={styles.main}>
            <TouchableOpacity onPress={() => { Actions.pop(); }}>
                <Form>
                    <Icon
                        name='ios-arrow-back'
                        size={30}
                        style={styles.icon}
                    />
                </Form>
            </TouchableOpacity>
            <Form style={styles.txtBox}>
                <Label style={styles.txt}>{title}</Label>
            </Form>
        </Form>
    </Form>
);

const styles = {
    icon: {
        color: colors.black,
    },
    main: {
        marginLeft: 20,
        height: 45,
        flexDirection: 'row',
        alignItems: 'center',
    },
    txtBox: {
        flex: 1,
    },
    txt: {
        alignSelf: 'center',
        fontSize: 24,
        color: colors.black,
        marginRight: '15%'
    },
    line: {
        borderBottomWidth: 1,
        borderColor: colors.gray
    }
}

export default NavbarAux;
