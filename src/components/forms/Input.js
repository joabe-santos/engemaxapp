import React from 'react';
import { Input, Item, Label, Form,  } from 'native-base';

const InputText = ({ title, edit, ...props }) => (
            <Item stackedLabel >
                <Label>{title}</Label>
                <Input {...props} disabled={edit} style={edit && styles.disabled}/>
            </Item>
);

const styles = {
    disabled: {
        opacity: 0.6
    },
} 

export default InputText;
