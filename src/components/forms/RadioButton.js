import React from 'react';
import { TouchableOpacity, Platform, Dimensions } from 'react-native';
import { Label, Form } from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons'
import { colors } from '../../utils/';

const RadioButton = ({ value, press, label, title, edit }) => (
    <Form>
        <Label>{title}</Label>
        <TouchableOpacity disabled={edit} style={[styles.containerTouch, edit && styles.disabled ]} onPress={press}>
            <Form style={styles.circle}>
                {value && <Icon size={30} style={styles.img} name="ios-checkmark" />}
            </Form>
            <Label style={styles.txtlabel}> {label}</Label>
        </TouchableOpacity>
    </Form>
)


const styles = {
    containerTouch: {
        flexDirection: 'row'
    },
    circle: {
        ...Platform.select({
            ios: {
                height: 24,
                width: 24,
                borderWidth: 1,
                borderColor: colors.black,
                borderRadius: 12
            },
            android: {
                alignItems: 'center',
                justifyContent: 'center',
                height: 24,
                width: 24,
                borderWidth: 1,
                borderColor: colors.black,
                borderRadius: 12
            },
        })
    },
    disabled: {
        opacity: 0.6
    },
    img: {
        ...Platform.select({
            ios: {
                bottom: -6,
                left: 4,
                position: 'absolute',
                color: colors.blueMain
            },
            android: {
                color: colors.blueMain
            }
        })


    },
    txtlabel: {
        color: colors.black
    }
}

export default RadioButton;

