import React, { Component } from 'react';
import { ActivityIndicator, View, StyleSheet } from 'react-native';
import { colors } from '../../utils';

class Spinner extends Component {
   state = { animating: true }
  
   render() {
      const animating = this.state.animating
      return (
         <View style = {styles.container}>
            <ActivityIndicator
               animating = {animating}
               color = {colors.blueMain}
               size = "large"
               style = {styles.activityIndicator}/>
         </View>
      )
   }
}
export default Spinner

const styles = StyleSheet.create ({
   container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: 70
   },
   activityIndicator: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      height: 80
   }
})