import { ScrollView } from 'react-native';

const Container = ({ noPadding }) => (
    <View style={[styles.default, noPadding]}></View>
);

const styles = {
  default: {
      padding: 10
  }
};