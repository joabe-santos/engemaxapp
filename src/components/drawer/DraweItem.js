import React from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native';

import icons from '../../assets/icons';
import colors from '../../utils/colors';

const MenuItem = ({onPress, icon, text, rightElement}) => (
  <View>
    <TouchableOpacity onPress={onPress}>
      <View style={styles.itemContainer}>
        {icon !== '' && (
          <Image
          resizeMode='contain'
            source={icons[icon]}
            style={{ marginRight: 8, marginLeft: 15, width: 24, height: 24 }}
          />
        )}
        <View style={[styles.row, { paddingLeft: icon === '' ? 50 : 0 }]}>
          <Text style={styles.menuText}>
            {text}
          </Text>
          <View>
            {rightElement}
          </View>
        </View>
      </View>
    </TouchableOpacity>
  </View>
);

const styles = {
  itemContainer: {
    flexDirection: 'row',
    padding: 5,
    paddingBottom: 10,
    paddingTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  menuText: {
    color: colors.blueMain,
    fontSize: 20,
    fontFamily: 'roboto'
  },
};

MenuItem.defaultProps = {
  rightElement: <View />,
  icon: '',
};

export default MenuItem;
