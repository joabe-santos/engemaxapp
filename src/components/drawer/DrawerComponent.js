import React from 'react';
import {  Platform, Dimensions, StatusBar, Text } from 'react-native';
import { Drawer, Container } from 'native-base';
import Menu from './DrawerMenu';
import Routes from '../../Routes'
import { colors } from '../../utils';

class DrawerNavigation extends React.Component {
 
  menuComponent = () => {
    return (
      <Menu closeMenu={this.closeMenu} />
    );
  }

  closeMenu = () => {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('default', 'fade');
    }
   
    this.drawer._root.close();
  }
  
  openMenu = () => {
    if (Platform.OS === 'ios') {
      StatusBar.setBarStyle('default', 'fade');
    }
    
    this.drawer._root.open();
  }

  render() {
    return (
      <Drawer
        ref={(ref) => { this.drawer = ref; }}
        onClose={this.closeMenu}
        captureGestures="open"
        tweenDuration={215}
        tweenEasing="easeInSine"
        type="displace"
        content={this.menuComponent()}
        openDrawerOffset={Math.ceil(Dimensions.get('window').width * 0.2)}
        tapToClose
      >
        <Container style={styles.container}>
          <Routes openMenu={this.openMenu} />
        </Container>
      </Drawer>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: colors.white
  }
};

export default DrawerNavigation;
