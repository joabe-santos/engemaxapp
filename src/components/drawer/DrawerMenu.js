import React, { Component } from 'react';
import { View, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import MenuItem from './DraweItem';
import DrawerHeader from './DraweHeader'
import { colors } from '../../utils';
import { clearUserAcess } from '../../utils/localStore';
import { imageValidate } from '../../utils/PhotoHelper';
import { cleanInspectiosList } from '../../screens/inspencao/store/actions';

class Menu extends Component {

  render() {
    const city = this.props.data.cidade === '' ? '' : this.props.data.cidade;
    const state = this.props.data.estado === '' ? '' : this.props.data.estado;
    return (
      <View style={styles.container}>
        <ScrollView>
          <View>
            <DrawerHeader 
              name={this.props.data.nome_fantasia}
              location={`${city}, ${state}`}
              img={imageValidate(this.props.data.imagem)}
              onPress={() => {
                Actions.user_data_screen();
                this.props.closeMenu();
              }}
            />
            <MenuItem
              icon='inspecao'
              text='Inspeções'
              onPress={() => { 
                Actions.inspencao();
                this.props.closeMenu();
              }}
            />
            <MenuItem
              icon='service'
              text='Serviços'
              onPress={() => { 
                Actions.services();
                this.props.closeMenu();
              }}
            />
            <MenuItem
              icon='bombas'
              text='Bombas'
              onPress={() => { 
                Actions.petrol();
                this.props.closeMenu();
              }}
            />
            <MenuItem
              icon='tank'
              text='Tanques'
              onPress={() => {
                Actions.tanks();
                this.props.closeMenu();
              }}
            />
            <MenuItem
              icon='filter'
              text='Filtros'
              onPress={() => {
                Actions.filters();
                this.props.closeMenu();
              }}
            />
            <MenuItem
              icon='key'
              text='Alterar senha'
              onPress={() => {
                Actions.change_pass();
                this.props.closeMenu();
              }}
            />
            <MenuItem
              icon='logout'
              text='Sair'
              onPress={() => {
                Actions.login();
                this.props.closeMenu();
                clearUserAcess();
                this.props.cleanInspectiosList();
              }}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = {
  container: {
    backgroundColor: colors.white,
    flex: 1,
  },
  division: {
    borderColor: '#CCCCD1',
    borderTopWidth: 2,
    borderBottomWidth: 2,
    paddingTop: 10,
    paddingBottom: 10,
    marginTop: 10,
    marginBottom: 10,
  },
  overlay: {
    right: 0,
    left: 0,
    top: 0,
    bottom: 0,
    position: 'absolute',
  }
};

const mapStateToProps = state => ({
    data: state.client.clientFilds,
})

export default connect(mapStateToProps, { cleanInspectiosList })(Menu);
