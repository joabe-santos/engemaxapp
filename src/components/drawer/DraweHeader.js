import React from 'react';
import { Image, View, Text, TouchableOpacity } from 'react-native';
import { colors } from '../../utils';

const DrawerHeader = ({ img, name, location, onPress }) => (
    <TouchableOpacity onPress={onPress} style={styles.main}>
        <Image style={styles.styleImg} source={img} />
        <View style={styles.txtBox}>
            <Text style={styles.txtName}>{name !== '' ? name : 'Crie seu Perfil'}</Text>
            <Text style={styles.txtLocation}>{location.toUpperCase()}</Text>
            <View style={styles.borderLine}></View>
        </View>        
    </TouchableOpacity>
);

const styles = {
    main: {
        flexDirection: 'row',
        marginTop: 40,
        marginBottom: 40,
        marginLeft: 8
    },
    txtBox: {
        justifyContent: 'center',
        marginLeft: 8,
        width: '100%'
    },
    styleImg: {
        height: 50, 
        width: 50,
    },
    txtName: {
        fontSize: 20,
        color: colors.blueMain,
        fontFamily: 'roboto',
        fontWeight: '300'
    },
    txtLocation: {
        fontSize: 15,
        color: colors.gray,
        fontFamily: 'roboto',
    },
    borderLine: {
        height: 1,
        borderColor: colors.line,
        borderWidth: 0.5,
        marginTop: 8
        
    }
};

export default DrawerHeader;
