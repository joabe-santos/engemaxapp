import React, { Component } from 'react';
import { Container, Content, Button, Label, Spinner, Form } from 'native-base';
import { Alert } from 'react-native';
import { connect } from 'react-redux';

import { InputText, NavbarAux } from '../../components/forms';
import { colors } from '../../utils';
import { Actions } from 'react-native-router-flux';
import { getUserAcess, setUserAcess, clearUserAcess } from '../../utils/localStore';
import apiService from '../../services/baseApi';

class PasswordChange extends Component {

    componentDidMount() {
        Actions.change_pass({ title: this.props.data.nome_fantasia });
    }

    state = {
        currentPass: '',
        newPass: '',
        loading: false,
    }

    handleNewPass = async () => {
        const { currentPass, newPass } = this.state;
        this.setState({ loading: true });
        const acess = await getUserAcess();

        if (currentPass !== '' && newPass !== '') {
            if (currentPass !== newPass) {
                this.setState({ loading: false });
                Alert.alert('Erro!', 'Senha que você digitou não coincidem')
            }
            if (newPass.length < 6) {
                this.setState({ loading: false });
                Alert.alert('Erro!', 'Senha deve conter mais de 6 caracteres')
                return false
            }
            if (currentPass === newPass) {
                try {
                    if (acess !== null) {
                        const params = { password: newPass }
                        const response = await apiService.edit.editPass(acess.token, params);
                        console.log('gsfdgdfg', response)
                        if (response.status === 201) {
                            setUserAcess({ ...acess, password: newPass })
                            this.setState({ loading: false, currentPass: '', newPass: '' });
                            Alert.alert('Sucesso!', 'Senha Alterada com Sucesso')
                            Actions.inspencao();
                        }
                    }
                } catch (error) {
                    this.setState({ loading: false });
                    console.log(error.response)
                    Alert.alert('Erro!', 'Problema no envio.');
                }
            }
        }
        else {
            this.setState({ loading: false });
            Alert.alert('Erro!', 'Campos obrigratório');
        }

    }

    render() {
        const { currentPass, newPass } = this.state;
        return (
            <Container>
                <NavbarAux title='Alterar Senha' />
                <Content style={styles.space}>
                    <Form>
                        <InputText
                            value={currentPass}
                            title='Senha'
                            editable
                            autoCorrect={false}
                            onChangeText={value => { this.setState({ currentPass: value }) }}
                            multiline={false}
                            underlineColorAndroid='transparent'
                            secureTextEntry
                        />
                        <InputText
                            value={newPass}
                            title='Nova Senha'
                            editable
                            autoCorrect={false}
                            onChangeText={value => { this.setState({ newPass: value }) }}
                            multiline={false}
                            underlineColorAndroid='transparent'
                            secureTextEntry
                        />
                    </Form>
                </Content>
                <Button onPress={() => {
                    this.handleNewPass();
                }}
                    style={styles.btn}>
                    {this.state.loading ?
                        <Spinner color={colors.white} /> :
                        <Label style={{ color: colors.white }}>Atualizar</Label>
                    }
                </Button>
            </Container>
        )
    }
}

const styles = {
    btn: {
        borderRadius: 20,
        width: '80%',
        height: 44,
        marginTop: 110,
        marginBottom: 20,
        backgroundColor: colors.green,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    space: {
        margin: 8
    }
}

const mapToProp = state => ({
    data: state.client.clientFilds,
});

export default connect(mapToProp)(PasswordChange);
