import React, { Component } from 'react';
import {
  Container,
  Content,
  Form,
  Text,
} from 'native-base';
import { StatusBar, Image, TouchableOpacity, Alert, BackHandler } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { colors } from '../../utils';
import InspectionComponent from '../../components/inspection/InspectionComponent';
import icons from '../../assets/icons';
import { getUserAcess } from '../../utils/localStore';
import { changeClientFild } from '../client/store/actions';
import { setInspectionMonth, getInspectionsAction, getInspectionDetailAction, cleanInspectiosDetail, cleanInspectios } from './store/actions';
import baseApi from '../../services/baseApi';
import Spinner from '../../components/forms/Spinner';

class InspecaoScreen extends Component {

  async componentWillMount() {
    this.props.getInspectionsAction();
    const acess = await getUserAcess();
    try {
      const response = await baseApi.register.getUser(acess.token);
      if (response.status === 200) {
        this.props.changeClientFild(response.data);
      }
    } catch(error) {
      Alert.alert('Erro!','Erro ao recuperar dados do servidor');
    }
    Actions.inspencao({ title: this.props.data.nome_fantasia });
    BackHandler.removeEventListener('hardwareBackPress', () => this.handleBackButtonClick())
    setTimeout(() => {
      this.getMonthsList();
    }, 300)
  }

  componentDidMount() {
    
    BackHandler.addEventListener('hardwareBackPress', () => this.handleBackButtonClick())
  }

  handleBackButtonClick = () => {
    if (Actions.currentScene === 'inspencao') {
      return true
    }
  };

  getMonthsList = () => {
    const { inpectionsList } = this.props;
    inpectionsList.map((it) => {
      const mes = it.data_cad.slice(5, 7);
      const arr = [...this.props.meses[mes]];
      arr.push(it);
      this.props.setInspectionMonth({[mes]: arr});
    })
  }

  goDetail = (hash) => {
    this.props.getInspectionDetailAction(hash);
    setTimeout(() => {
      Actions.first_inspection({ clean: true });
    }, 100);
  }

  navigate = () => {
    this.props.cleanInspectiosDetail();
    this.props.cleanInspectios();
    setTimeout(() => {
      Actions.first_inspection({ clean: false });
    }, 50);
  }

  render() {
    if(this.props.loadingInspections) {
      return(
        <Spinner />
      )
    }
    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor={colors.blueMain} />
        <Form style={styles.formContainer}>
          <Text style={styles.txtHeader}>Inspeções</Text>
        </Form>
        <Content style={{ marginBottom: 10 }}>
          {this.props.meses['01'].length > 0  && <InspectionComponent goDetails={this.goDetail} item={this.props.meses['01']} />}
          {this.props.meses['02'].length > 0  && <InspectionComponent goDetails={this.goDetail} item={this.props.meses['02']} />}
          {this.props.meses['03'].length > 0  && <InspectionComponent goDetails={this.goDetail} item={this.props.meses['03']} />}
          {this.props.meses['04'].length > 0  && <InspectionComponent goDetails={this.goDetail} item={this.props.meses['04']} />}
          {this.props.meses['05'].length > 0  && <InspectionComponent goDetails={this.goDetail} item={this.props.meses['05']} />}
          {this.props.meses['06'].length > 0  && <InspectionComponent goDetails={this.goDetail} item={this.props.meses['06']} />}
          {this.props.meses['07'].length > 0  && <InspectionComponent goDetails={this.goDetail} item={this.props.meses['07']} />}
          {this.props.meses['08'].length > 0  && <InspectionComponent goDetails={this.goDetail} item={this.props.meses['08']} />}
          {this.props.meses['09'].length > 0  && <InspectionComponent goDetails={this.goDetail} item={this.props.meses['09']} />}
          {this.props.meses['10'].length > 0  && <InspectionComponent goDetails={this.goDetail} item={this.props.meses['10']} />}
          {this.props.meses['11'].length > 0  && <InspectionComponent goDetails={this.goDetail} item={this.props.meses['11']} />}
          {this.props.meses['12'].length > 0  && <InspectionComponent goDetails={this.goDetail} item={this.props.meses['12']} />}
        </Content>
        <Form style={{ alignItems: 'center', marginBottom: 20 }}>
          <TouchableOpacity onPress={() => this.navigate()}>
            <Image style={{ height: 40, width: 40 }} source={icons['add_button']} />
          </TouchableOpacity>
        </Form>
      </Container>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff'
  },
  formContainer: {
    alignItems: 'center',
    paddingBottom: 5
  },
  txtHeader: {
    fontFamily: 'Roboto',
    fontSize: 24,
    color: colors.blueMain,
  }
}

const mapStateToProps = state => ({
  meses: state.inspection.meses,
  inpectionsList: state.inspection.inpectionsList,
  updateList: state.inspection.updateList,
  loadingInspections: state.inspection.loadingInspections,
  data: state.client.clientFilds,
});

export default connect(mapStateToProps, {
  changeClientFild,
  setInspectionMonth,
  getInspectionsAction,
  getInspectionDetailAction,
  cleanInspectiosDetail,
  cleanInspectios
})(InspecaoScreen);
