import React from 'react';
import { Container, Content, Button, Label, Input, Form, Spinner } from 'native-base';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';
import { connect } from 'react-redux';
import { create } from 'apisauce';

import { NavbarAux } from '../../components/forms';
import CountPage from '../../components/forms/CountPage';
import { colors } from '../../utils';
import { changeThirdInspection, setInspectionMonth, getInspectionsAction, clearMonths, cleanInspectios } from './store/actions';
import { getUserAcess } from '../../utils/localStore';

class ThirdInspection extends React.Component {
    state = {
        loading: false,
    }

    componentWillMount() {
        this.checkForInspection();
        Actions.third_inspection({ title: this.props.data.nome_fantasia });
    }

    checkForInspection = () => {
        const { inspectionDetail } = this.props;
        if (inspectionDetail !== null) {
            this.props.changeThirdInspection({ 
                message: inspectionDetail.observacao_extra,
                name: inspectionDetail.nome_responsavel
            });
        }
    }

    saveInspection = async () => {
        this.props.clearMonths();
        this.setState({ loading: true });
        const acess = await getUserAcess();
        const formDada = new FormData();

        formDada.append('nome', `Inspeção interna 0${this.props.inspectionQtd + 1}`);
        formDada.append('limpeza_canaletas', this.props.inspectionData.limpeza_canaletas);
        formDada.append('limpeza_canaletas_obs', this.props.inspectionData.limpeza_caixa_sepa_semana_obs);
        const photosCanaletas = this.props.inspectionData.limpeza_canaletas_imagens;
        photosCanaletas.forEach((image) => {
            image.forEach(it => {
                formDada.append('limpeza_canaletas_imagens', {
                    uri: it,
                    type: 'image/jpeg',
                    name: 'image.jpg'
                }
                )
            })
        });

        formDada.append('sec_limp_bocas', this.props.inspectionData.sec_limp_bocas);
        formDada.append('sec_limp_bocas_obs', this.props.inspectionData.sec_limp_bocas_obs);
        const photosLimpeza = this.props.inspectionData.sec_limp_bocas_imagens;
        photosLimpeza.forEach((image) => {
            image.forEach(it => {
                formDada.append('sec_limp_bocas_imagens', {
                    uri: it,
                    type: 'image/jpeg',
                    name: 'image.jpg'
                }
                )
            })
        });

        formDada.append('bombas_abast', this.props.inspectionData.bombas_abast);
        formDada.append('bombas_abast_obs', this.props.inspectionData.bombas_abast_obs);
        const photosAbastecimentos = this.props.inspectionData.bombas_abast_imagens;
        photosAbastecimentos.forEach((image) => {
            image.forEach(it => {
                formDada.append('bombas_abast_imagens', {
                    uri: it,
                    type: 'image/jpeg',
                    name: 'image.jpg'
                }
                )
            })
        });

        formDada.append('filtros_diesel_vazamento', this.props.inspectionData.filtros_diesel_vazamento);
        formDada.append('filtros_diesel_vazamento_obs', this.props.inspectionData.filtros_diesel_vazamento_obs);
        const photosVazamento = this.props.inspectionData.filtros_diesel_vazamento_imagens;
        photosVazamento.forEach((image) => {
            image.forEach(it => {
                formDada.append('filtros_diesel_vazamento_imagens', {
                    uri: it,
                    type: 'image/jpeg',
                    name: 'image.jpg'
                }
                )
            })
        });

        formDada.append('limpeza_caixa_sepa_semana', this.props.inspectionData.limpeza_caixa_sepa_semana);
        formDada.append('limpeza_caixa_sepa_semana_obs', this.props.inspectionData.limpeza_caixa_sepa_semana_obs);
        const photosLimpezaCaixa = this.props.inspectionData.limpeza_caixa_sepa_semana_imagens;
        photosLimpezaCaixa.forEach((image) => {
            image.forEach(it => {
                formDada.append('limpeza_caixa_sepa_semana_imagens', {
                    uri: it,
                    type: 'image/jpeg',
                    name: 'image.jpg'
                }
                )
            })
        });

        formDada.append('separacao_residuos', this.props.inspectionData.separacao_residuos);
        formDada.append('separacao_residuos_obs', this.props.inspectionData.separacao_residuos_obs);
        const photosSeparacao = this.props.inspectionData.separacao_residuos_imagens;
        photosSeparacao.forEach((image) => {
            image.forEach(it => {
                formDada.append('separacao_residuos_imagens', {
                    uri: it,
                    type: 'image/jpeg',
                    name: 'image.jpg'
                }
                )
            })
        });

        formDada.append('observacao_extra', this.props.finalInspection.message);
        formDada.append('nome_responsavel', this.props.finalInspection.name);

        const api = create({
            baseURL: 'https://engemax.leaderaplicativos.com.br/api/v1/',
            headers: { 'Authorization': `JWT ${acess.token}` }
        });

        api.post('/inspecoes-internas/', formDada, {
        }).then((response) => {
                if (response.status === 200 || response.status === 201) {
                    this.props.getInspectionsAction();
                    setTimeout(() => {
                        this.props.inpectionsList.map((it) => {
                            const mes = it.data_cad.slice(5, 7);
                            const arr = [...this.props.meses[mes]];
                            arr.push(it);
                            this.props.setInspectionMonth({[mes]: arr});
                        });
                        this.setState({ loading: false });
                        this.props.cleanInspectios();
                        Actions.inspencao(); 
                    }, 1000);
                }
            })
            .catch((erro) => {
                this.setState({ loading: false });
                alert('Erro ao salvar inspeção.')
            });
    
    }

    validateFilds = () => {
        if (this.props.finalInspection.name === '') {
            alert('O campo (Nome) é obrigatório');
        } else {
            this.saveInspection();
        }
    }

    render() {
        const { inspectionDetail } = this.props;
        const header = inspectionDetail !== null ? inspectionDetail.nome : `Inspeção interna 0${this.props.inspectionQtd + 1}`; 
        const allTime = new Date();
        const date = moment().format('DD/MM/YYYY');
        const time = `${allTime.getHours()}:${allTime.getMinutes()}`
        return (
            <Container>
                <NavbarAux title={header} />
                <Content style={styles.wrapper}>
                    <Form style={[styles.firstBox, styles.boxInput]}>
                        <Label style={styles.description}>Deseja informar alguma observação extra importante?</Label>
                        <Input
                            value={this.props.finalInspection.message}
                            editable={inspectionDetail !== null ? false : true}
                            autoCorrect={false}
                            onChangeText={txt => this.props.changeThirdInspection({ message: txt })}
                            multiline
                            underlineColorAndroid='transparent'
                            textAlignVertical='top'
                            placeholderTextColor={colors.gray}
                            style={{ color: colors.gray }}
                        />
                    </Form>
                    <Form style={styles.firstBox}>
                        <Label style={[styles.description, styles.description2]}>
                            Você confirma que é responsável pela manutenção dos itens dessa inspeção
                            e que à realizou dentro das normas estabelecidas no treinamento?.
                        </Label>
                        <Label style={styles.label}>Nome</Label>
                        <Input
                            value={this.props.finalInspection.name}
                            editable={inspectionDetail !== null ? false : true}
                            autoCorrect={false}
                            onChangeText={txt => this.props.changeThirdInspection({ name: txt })}
                            multiline
                            underlineColorAndroid='transparent'
                            textAlignVertical='top'
                            placeholderTextColor={colors.gray}
                            style={{ color: colors.gray }}
                        />
                    </Form>
                    <Form style={styles.firstBox}>
                        <Label style={styles.label}>Data e hora</Label>
                        <Label
                            style={{ color: colors.gray, marginTop: 8 }}
                        >
                            {date}  {time}
                        </Label>
                    </Form>
                </Content>
                <CountPage forward={true} act3={{ backgroundColor: colors.green }} />
                <Button
                    onPress={() => this.validateFilds()}
                    style={[styles.btn, inspectionDetail === null ? {} : styles.disable]}
                    disabled={inspectionDetail !== null ? true : false}
                >
                    {this.state.loading ?
                        <Spinner size='large' color={colors.white} /> :
                        <Label style={styles.btnTxt}>Finalizar</Label>
                    }
                </Button>
            </Container>
        )
    }
}

const styles = {
    wrapper: {
        paddingLeft: 14,
        paddingRight: 14,
        paddingBottom: 10
    },
    firstBox: {
        borderBottomWidth: 1,
        borderBottomColor: colors.line,
    },
    boxInput: {
        height: 180,
        paddingTop: 10
    },
    btn: {
        borderRadius: 20,
        width: '80%',
        height: 44,
        marginTop: 10,
        marginBottom: 20,
        backgroundColor: colors.green,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    btnTxt: {
        color: colors.white
    },
    description: {
        color: colors.blueMain,
        fontFamily: 'Helvetica',
        fontSize: 15
    },
    label: {
        marginTop: 14,
        color: colors.black
    },
    description2: {
        marginTop: 10
    },
    disable: {
        opacity: 0.7
    }
};

const mapStateToPrpos = state => ({
    inspectionData: state.inspection.inspectionData,
    finalInspection: state.inspection.finalInspection,
    inspectionQtd: state.inspection.inspectionQtd,
    inpectionsList: state.inspection.inpectionsList,
    meses: state.inspection.meses,
    inspectionDetail: state.inspection.inspectionDetail,
    data: state.client.clientFilds,
});

export default connect(mapStateToPrpos, {
    changeThirdInspection,
    setInspectionMonth,
    getInspectionsAction,
    clearMonths,
    cleanInspectios
})(ThirdInspection);
