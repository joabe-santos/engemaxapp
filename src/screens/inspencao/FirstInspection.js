import React from 'react';
import { Alert, Modal, ImageBackground, TouchableOpacity, Text } from 'react-native';
import { Container, Content, Button, Label, Form } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { NavbarAux } from '../../components/forms';
import InspectionItem from './InspectionItem';
import CountPage from '../../components/forms/CountPage';
import { PhotoHelper } from '../../utils/PhotoHelper';
import { colors } from '../../utils';
import { setInspection, changeClear, changeDry, changeInternal } from './store/actions';


class FirstInspection extends React.Component {
    componentWillMount() {
        this.checkForInspection();
        Actions.first_inspection({ title: this.props.data.nome_fantasia });
    }

    state = {
        navigate: false,
        update: 0,
        camera: false,
        click: false,
        modalVisible: false,
        img: null
    }

    checkForInspection = () => {
        const { inspectionDetail } = this.props;
        if (inspectionDetail !== null) {
            this.props.changeClear({
                imageslocal: inspectionDetail.limpeza_canaletas_imagens,
                cleanYes: inspectionDetail.limpeza_canaletas,
                message: inspectionDetail.limpeza_canaletas_obs
            })
            this.props.changeDry({
                imageslocal: inspectionDetail.sec_limp_bocas_imagens,
                dryYes: inspectionDetail.sec_limp_bocas,
                message: inspectionDetail.sec_limp_bocas_obs
            });
            this.props.changeInternal({
                imageslocal: inspectionDetail.bombas_abast_imagens,
                internalYes: inspectionDetail.bombas_abast,
                message: inspectionDetail.bombas_abast_obs
            })
        }
    }

    clearList = () => {
        const { imageslocal, cleanYes } = this.props.listClean;
        return [{
                title:'Realizou a limpeza das caneletas de contenção da pista?',
                status: cleanYes,
                images: imageslocal,
                pressYesNo: () => this.props.changeClear({ cleanYes: !cleanYes }, this.checkButtom()),
                pressPhoto: () => this.setState({ update: 1 }, this.setupCamera()),
                removeImg: this.removeListCleanImage
            }]
    }
    listDry = () => {
        const { imageslocal, dryYes } = this.props.listDry;
        return [{
                title:'Realizou a secagem e limpeza das bocas de descarga?',
                status: dryYes,
                images: imageslocal,
                pressYesNo: () => this.props.changeDry({ dryYes: !dryYes }, this.checkButtom()),
                pressPhoto: () => this.setState({ update: 2 }, this.setupCamera()),
                removeImg: this.removeListDryImage
            }]
    }
    listInternalCheck = () => {
        const { imageslocal, internalYes } = this.props.listInternal;
        return [{
                title: 'Verificou internamente as bombas de Abastecimento?',
                status: internalYes,
                images: imageslocal,
                pressYesNo: () => this.props.changeInternal({ internalYes: !internalYes }, this.checkButtom()),
                pressPhoto: () => this.setState({ update: 3 }, this.setupCamera()),
                removeImg: this.removeListInternalImage
            }]
    } 

    setupCamera = () => {
        PhotoHelper(data => {
            this.setupImage(data);
        });
    } 

    checkButtom = () => {
        const { listClean, listDry, listInternal } = this.props;
            if ((listClean.imagesClean.length > 0) || (listClean.message !== '')) {
                if ((listDry.imagesDry.length > 0) || (listDry.message !== '')){
                    if ((listInternal.imagesInternal.length > 0) || (listInternal.message !== '')) {
                        this.setState({ click: true });
                    }
                }
        }
    }

    removeListCleanImage = item => {
        this.setState({ img: item, modalVisible: true, update: 1 } );
    }
    removeListDryImage = item => {
        this.setState({ img: item, modalVisible: true, update: 2 } );
    }
    removeListInternalImage = item => {
        this.setState({ img: item, modalVisible: true, update: 3 } );
    }

    removeImgs = (img) => {
        switch(this.state.update) {
            case 1:
                this.rmClean(img)
            break;
            case 2:
                this.rmDry(img)
             break;  
             case 3:
                this.rmInternal(img)
             break;
             default: 1; 
        }
    }
    rmClean = (item) => {
        const { imageslocal, imagesClean } = this.props.listClean;
        const index = imageslocal.findIndex(img => img === item);
        imageslocal.splice(index, 1);
        this.props.changeClear({ imageslocal, imagesClean: imageslocal });
        this.setState({ modalVisible: false });
    }
    rmDry = (item) => {
        const { imageslocal, imagesDry } = this.props.listDry;
        const index = imageslocal.findIndex(img => img === item);
        imageslocal.splice(index, 1);
        this.props.changeDry({ imageslocal, imagesDry: imageslocal });
        this.setState({ modalVisible: false });
    }
    rmInternal = (item) => {
        const { imageslocal, imagesInternal } = this.props.listInternal;
        const index = imageslocal.findIndex(img => img === item);
        imageslocal.splice(index, 1);
        this.props.changeInternal({ imageslocal, imagesInternal: imageslocal });
        this.setState({ modalVisible: false });
    }

    setupImage = (img) => {
        switch(this.state.update) {
            case 1:
                const arrl1 = [...this.props.listClean.imageslocal];
                const arr641 = [...this.props.listClean.imagesClean];
                arrl1.push(img);
                arr641.push(img);
                this.setState({ camera: false });
                this.props.changeClear({ imageslocal: arrl1, imagesClean: arr641 });
            break;
            case 2:
                const arrl2 = [...this.props.listDry.imageslocal];
                const arr642 = [...this.props.listDry.imagesDry];
                arrl2.push(img);
                arr642.push(img);
                this.setState({ camera: false });
                this.props.changeDry({ imageslocal: arrl2, imagesDry: arr642 });
             break;  
             case 3:
                const arrl3 = [...this.props.listInternal.imageslocal];
                const arr643= [...this.props.listInternal.imagesInternal];
                arrl3.push(img);
                arr643.push(img)
                this.setState({ camera: false });
                this.props.changeInternal({ imageslocal: arrl3, imagesInternal: arr643 });
                setTimeout(() => {
                    this.checkButtom();
                }, 100);
             break;
             default: 1; 
        }
    }

    saveData = () => {
        const { listClean, listDry, listInternal, inspectionDetail } = this.props;
        params = {
            nome: 'Nova Inspeção',
            limpeza_canaletas: listClean.cleanYes,
            limpeza_canaletas_obs: listClean.message,
            limpeza_canaletas_imagens: [listClean.imagesClean],
            sec_limp_bocas: listDry.dryYes,
            sec_limp_bocas_obs: listDry.message,
            sec_limp_bocas_imagens: [listDry.imagesDry],
            bombas_abast: listInternal.internalYes,
            bombas_abast_obs: listInternal.message,
            bombas_abast_imagens: [listInternal.imagesInternal]
        }

        if (inspectionDetail === null) {
            if ((listClean.imagesClean.length > 0) || (listClean.message !== '')) {
                if ((listDry.imagesDry.length > 0) || (listDry.message !== '')){
                    if ((listInternal.imagesInternal.length > 0) || (listInternal.message !== '')) {
                        this.props.setInspection(params);
                        Actions.second_inspection();
                    }
                }
            }
        } else {
             Actions.second_inspection();
        }
    }

    goForward = () => {
        const { listClean, listDry, listInternal, inspectionDetail } = this.props;
        params = {
            nome: 'Nova Inspeção',
            limpeza_canaletas: listClean.cleanYes,
            limpeza_canaletas_obs: listClean.message,
            limpeza_canaletas_imagens: [listClean.imagesClean],
            sec_limp_bocas: listDry.dryYes,
            sec_limp_bocas_obs: listDry.message,
            sec_limp_bocas_imagens: [listDry.imagesDry],
            bombas_abast: listInternal.internalYes,
            bombas_abast_obs: listInternal.message,
            bombas_abast_imagens: [listInternal.imagesInternal],
        }
        if (inspectionDetail === null) {
            if ((listClean.imagesClean.length > 0) || (listClean.message !== '')) {
                if ((listDry.imagesDry.length > 0) || (listDry.message !== '')){
                    if ((listInternal.imagesInternal.length > 0) || (listInternal.message !== '')) {
                        this.props.setInspection(params);
                        return true
                    }
                }
            }
        } else { 
            return true
       }
        return false
    }

    setModal = () => {
        const { img } = this.state;
        return (
            <Modal
              visible={this.state.modalVisible}
              style={{ marginTop: 60 }}
              onRequestClose={() => {}}
            >
            <Form>
                <ImageBackground style={{ width: '100%', height: '100%' }} source={{ uri: img }}>
                <Form style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity
                        onPress={() => this.removeImgs(img)}
                        style={styles.capture}
                    >
                        <Text style={styles.txtBtn}> EXCLUIR IMAGEM </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.setState({ modalVisible: false })}
                        style={styles.capture}
                    >
                        <Text style={styles.txtBtn}> CANCELAR </Text>
                    </TouchableOpacity>
                </Form>
                </ImageBackground>
            </Form>
            </Modal>
        )
    }

    render() {
        const { inspectionDetail } = this.props;
        const header = inspectionDetail !== null ? inspectionDetail.nome : `Inspeção interna 0${this.props.inspectionQtd + 1}`; 
        const { click } = this.state;
        /*if (this.state.camera) {
            return (
                <PhotoHelper
                    cancel={() => this.setState({ camera: false })}
                    setImage={this.setupImage}
                /> 
            )
        }*/
        return (
            <Container>
                <NavbarAux title={header} />
                <Content>
                    {this.setModal()}
                    <InspectionItem
                        list={this.clearList()}
                        value={this.props.listClean.message}
                        editable={inspectionDetail !== null ? false : true}
                        autoCorrect={false}
                        onChangeText={txt => this.props.changeClear({ message: txt }, this.checkButtom())}
                        placeholder='Digite...'
                        multiline
                        underlineColorAndroid='transparent'
                        textAlignVertical='top'
                        placeholderTextColor={colors.gray}
                        style={{ color: colors.gray }}
                        edit={inspectionDetail !== null ? true : false}
                    />
                    <InspectionItem 
                        list={this.listDry()}
                        value={this.props.listDry.message}
                        editable={inspectionDetail !== null ? false : true}
                        autoCorrect={false}
                        onChangeText={txt => this.props.changeDry({ message: txt }, this.checkButtom())}
                        placeholder='Digite...'
                        multiline
                        underlineColorAndroid='transparent'
                        textAlignVertical='top'
                        placeholderTextColor={colors.gray}
                        style={{ color: colors.gray }}
                        edit={inspectionDetail !== null ? true : false}
                    />
                    <InspectionItem
                        list={this.listInternalCheck()}
                        value={this.props.listInternal.message}
                        editable={inspectionDetail !== null ? false : true}
                        autoCorrect={false}
                        onChangeText={txt => this.props.changeInternal({ message: txt }, this.checkButtom())}
                        placeholder='Digite...'
                        multiline
                        underlineColorAndroid='transparent'
                        textAlignVertical='top'
                        placeholderTextColor={colors.gray}
                        style={{ color: colors.gray }}
                        edit={inspectionDetail !== null ? true : false}
                    />
                </Content>
                <CountPage inspecaoQtd={this.props.inspecaoQtd} forward={this.goForward()} act1={{ backgroundColor: colors.green }} />
                <Button
                    disabled={click ? false : true}
                    onPress={() => this.saveData()} 
                    style={[styles.btn, !click ? styles.disable : {}]}>
                    <Label style={styles.btnTxt}>Salvar e ir para o próximo</Label>
                </Button>
            </Container>
        )
    }
}

const styles = {
    wrapper: {
        padding: 14
    },
    btn: {
        borderRadius: 20,
        width: '80%',
        height: 44,
        marginTop: 10,
        marginBottom: 20,
        backgroundColor: colors.green,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
    },
    btnTxt: {
        color: colors.white
    },
    disable: {
        opacity: 0.7
    },
    capture: {
        width: '40%',
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 6,
        paddingHorizontal: 8,
        margin: 15,
        alignItems: 'center',
        justifyContent: 'center'
      },
      txtBtn: {
        fontSize: 14,
        color: colors.blueMain
      },
};

const mapStateToProps = state => ({
    listClean: state.inspection.listClean,
    listDry: state.inspection.listDry,
    listInternal: state.inspection.listInternal,
    inspectionQtd: state.inspection.inspectionQtd,
    inspectionDetail: state.inspection.inspectionDetail,
    data: state.client.clientFilds,
});

export default connect(mapStateToProps, {
    setInspection,
    changeClear,
    changeDry,
    changeInternal
})(FirstInspection);
