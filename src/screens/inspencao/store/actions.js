import baseApi from '../../../services/baseApi';
import { getUserAcess } from '../../../utils/localStore';

export const types = {
    inspections_data: 'inspections/inspections_data',
    change_filter_list: 'change_filter_list',
    change_list_box: 'change_list_box',
    change_separation_list: 'change_separation_list',
    change_third_inspection: 'change_third_inspection',
    cahnge_camera: 'cahnge_camera',
    inspection_qtd: 'inspection_qtd',
    inspection_months: 'inspection_months',
    inspections_list: 'inspections_list',
    clear_months: 'clear_months',
    loading_inspections: 'loading_inspections',
    set_inspection_detail: 'set_inspection_detail',
    change_clear: 'change_clear',
    change_dry: 'change_dry',
    change_internal: 'change_internal',
    clean_inspectios: 'clean_inspectios',
    clean_inspectios_detail: 'clean_inspectios_detail',
    clean_inspectios_list: 'clean_inspectios_list'
};

export const getInspectionsAction = () => {
    return async (dispatch) => {
        setLoadingInspections(dispatch, true);
        try {
            const acess = await getUserAcess();
            if(acess !== null) {
            const response = await baseApi.inspecoes.getInspecoes(acess.token);
            if (response.status === 200) {
                setLoadingInspections(dispatch, false);
                setInspectionQtd(dispatch, response.data.length);
                setInspectionsList(dispatch, response.data);
                }
            }
        } catch(error) {
            console.log('erro busca inspecao', error.response);
            alert('Erro ao buscar inspeções.');
            setLoadingInspections(dispatch, false);
        }
    }
}

export const getInspectionDetailAction = (hash) => {
    return async (dispatch) => {
        try {
            const acess = await getUserAcess();
            if (acess !== null) {
                const response = await baseApi.inspecoes.getInspectionDetail(acess.token, hash);
                if(response.status === 200) {
                    setInspectionDetail(dispatch, response.data);
                    console.log('response detail inspection::::', response);
                }
                
            }
        } catch(erro) {
            console.log('erro response detail::::', erro.response);
        }
    }
}

const setInspectionDetail = (dispatch, payload) => {
    dispatch({
        type: types.set_inspection_detail,
        payload
    });
};

export const setInspection = (payload) => ({
    type: types.inspections_data,
    payload
});

export const changefilterList = (payload) => ({
    type: types.change_filter_list,
    payload
});

export const changeListBox = (payload) => ({
    type: types.change_list_box,
    payload
});

export const changeSeparationList = (payload) => ({
    type: types.change_separation_list,
    payload
});

export const changeThirdInspection = (payload) => ({
    type: types.change_third_inspection,
    payload
});

export const changeCamera = (payload) => ({
    type: types.change_third_inspection,
    payload
});

export const setInspectionQtd = (dispatch, payload) => {
    dispatch({
        type: types.inspection_qtd,
        payload
    })
};

export const setInspectionMonth = (payload) => ({
    type: types.inspection_months,
    payload
});

const setInspectionsList = (dispatch, payload) => {
    dispatch({
        type: types.inspections_list,
        payload
    });
};

export const clearMonths = () => ({
    type: types.clear_months,
});

const setLoadingInspections = (dispatch, payload) => {
    dispatch({
        type: types.loading_inspections,
        payload
    })
};

export const changeClear = payload => ({
    type: types.change_clear,
    payload
});
export const changeDry = payload => ({
    type: types.change_dry,
    payload
});
export const changeInternal = payload => ({
    type: types.change_internal,
    payload
});

export const cleanInspectios = () => ({
    type: types.clean_inspectios,
});

export const cleanInspectiosDetail = () => ({
    type: types.clean_inspectios_detail,
});

export const cleanInspectiosList = () => ({
    type: types.clean_inspectios_list,
});

