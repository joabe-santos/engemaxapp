import { types } from './actions';

const INITIAL_STATE = {
    inspectionData: null,
    listClean: {
        imagesClean: [],
        imageslocal: [],
        cleanYes: false,
        message: ''
    },
    listDry: {
        imagesDry: [],
        imageslocal: [],
        dryYes: false,
        message: ''
    },
    listInternal: {
        imagesInternal: [],
        imageslocal: [],
        internalYes: false,
        message: ''
    },
    listCheckFilters: {
        imagesFilters: [],
        imageslocal: [],
        cleanYes: false,
        message: ''
    },
    listCleanBox: {
        imagesCleanBox: [],
        imageslocal: [],
        dryYes: false,
        message: ''
    },
    listSeparation: {
        imagesSeparation: [],
        imageslocal: [],
        internalYes: false,
        message: ''
    },
    finalInspection: {
        message: '',
        name: ''
    },
    meses: {
        '01': [],
        '02': [],
        '03': [],
        '04': [],
        '05': [],
        '06': [],
        '07': [],
        '08': [],
        '09': [],
        '10': [],
        '11': [],
        '12': []
      },
    inspectionQtd: 0,
    inpectionsList: [],
    loadingInspections: false,
    inspectionDetail: null
}

const reducer = (state = INITIAL_STATE, { type, payload }) => {
    switch(type) {
        case types.inspections_data:
        return {
            ...state,
            inspectionData: {
                ...state.inspectionData,
                ...payload
            }
        }
        case types.change_clear: 
        return {
            ...state,
            listClean: {
                ...state.listClean,
                ...payload
            }
        }
        case types.change_dry:
        return {
            ...state,
            listDry: {
                ...state.listDry,
                ...payload
            }
        }
        case types.change_internal: 
        return {
            ...state,
            listInternal: {
                ...state.listInternal,
                ...payload
            }
        }
        case types.change_filter_list:
        return {
            ...state,
            listCheckFilters: {
                ...state.listCheckFilters,
                ...payload
            }
        }
        case types.change_list_box:
        return {
            ...state,
            listCleanBox: {
                ...state.listCleanBox,
                ...payload
            }
        }
        case types.change_separation_list:
        return {
            ...state,
            listSeparation: {
                ...state.listSeparation,
                ...payload
            }
        }
        case types.change_third_inspection:
        return {
            ...state,
            finalInspection: {
                ...state.finalInspection,
                ...payload
            }
        }
        case types.inspection_qtd: 
        return {
            ...state,
            inspectionQtd: payload
        }
        case types.inspection_months:
        return {
            ...state,
            meses: {
                ...state.meses,
                ...payload
            }
        }
        case types.inspections_list:
        return {
            ...state,
            inpectionsList: payload
        }
        case types.clear_months:
        return {
            ...state,
            meses: {
                ...state.meses,
                '01': [],
                '02': [],
                '03': [],
                '04': [],
                '05': [],
                '06': [],
                '07': [],
                '08': [],
                '09': [],
                '10': [],
                '11': [],
                '12': []
            }
        }
        case types.loading_inspections:
        return {
            ...state,
            loadingInspections: payload
        }
        case types.set_inspection_detail:
        return {
            ...state,
            inspectionDetail: payload
        }
        case types.clean_inspectios:
        return {
            ...state,
            listClean: {
                imagesClean: [],
                imageslocal: [],
                cleanYes: false,
                message: ''
            },
            listDry: {
                imagesDry: [],
                imageslocal: [],
                dryYes: false,
                message: ''
            },
            listInternal: {
                imagesInternal: [],
                imageslocal: [],
                internalYes: false,
                message: ''
            },
            listCheckFilters: {
                imagesFilters: [],
                imageslocal: [],
                cleanYes: false,
                message: ''
            },
            listCleanBox: {
                imagesCleanBox: [],
                imageslocal: [],
                dryYes: false,
                message: ''
            },
            listSeparation: {
                imagesSeparation: [],
                imageslocal: [],
                internalYes: false,
                message: ''
            },
            finalInspection: {
                message: '',
                name: ''
            },
            inspectionData: null
        }
        case types.clean_inspectios_detail:
        return {
            ...state,
            inspectionDetail: null
        }
        case types.clean_inspectios_list:
        return {
            ...state,
            inpectionsList: [],
            meses: {
                '01': [],
                '02': [],
                '03': [],
                '04': [],
                '05': [],
                '06': [],
                '07': [],
                '08': [],
                '09': [],
                '10': [],
                '11': [],
                '12': []
            }
        }
        default:
        return state
    }
}

export default reducer;
