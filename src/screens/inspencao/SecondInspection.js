import React from 'react';
import { Container, Content, Button, Label, Form } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Modal, ImageBackground, TouchableOpacity, Text} from 'react-native';

import { NavbarAux } from '../../components/forms';
import InspectionItem from './InspectionItem';
import CountPage from '../../components/forms/CountPage';
import { PhotoHelper } from '../../utils/PhotoHelper';
import { colors } from '../../utils';
import { setInspection, changefilterList, changeListBox, changeSeparationList } from './store/actions';


class ScoundInspection extends React.Component {
    state = {
        update: 0,
        camera: false,
        click: false,
        modalVisible: false,
        img: null
    }

    componentWillMount() {
        this.checkForInspection();
        Actions.second_inspection({ title: this.props.data.nome_fantasia });
    }

    checkForInspection = () => {
        const { inspectionDetail } = this.props;
        if (inspectionDetail !== null) {
            this.props.changefilterList({ 
                imageslocal: inspectionDetail.filtros_diesel_vazamento_imagens,
                cleanYes: inspectionDetail.filtros_diesel_vazamento,
                message: inspectionDetail.filtros_diesel_vazamento_obs
            });

            this.props.changeListBox({ 
                imageslocal: inspectionDetail.limpeza_caixa_sepa_semana_imagens,
                dryYes: inspectionDetail.limpeza_caixa_sepa_semana,
                message: inspectionDetail.limpeza_caixa_sepa_semana_obs
            });

            this.props.changeSeparationList({ 
                imageslocal: inspectionDetail.separacao_residuos_imagens,
                internalYes: inspectionDetail.separacao_residuos,
                message: inspectionDetail.separacao_residuos_obs
            });
        }
    }

    filterList = () => {
        const { imageslocal, cleanYes } = this.props.listCheckFilters;
        return [{
                title:'Verificou se os filtros de Diesel possuem vazamentos?',
                status: cleanYes,
                images: imageslocal,
                pressYesNo: () => this.props.changefilterList({ cleanYes: !cleanYes }, this.checkButtom()),
                pressPhoto: () => this.setState({ update: 1 }, this.setupCamera()),
                removeImg: this.removeFilterImage
            }]
    }
    listBox = () => {
        const { imageslocal, dryYes } = this.props.listCleanBox;
        return [{
                title:'Realizou a limpeza da Caixa Separadora essa semana?',
                status: dryYes,
                images: imageslocal,
                pressYesNo: () => this.props.changeListBox({ dryYes: !dryYes }, this.checkButtom()),
                pressPhoto: () => this.setState({ update: 2 }, this.setupCamera()),
                removeImg: this.removeBoxImage
            }]
    }
    separationList = () => {
        const { imageslocal, internalYes } = this.props.listSeparation;
        return [{
                title: 'Realizou a separação correta e organização dos residuos classe 1 nos tambores identificados?',
                status: internalYes,
                images: imageslocal,
                pressYesNo: () => this.props.changeSeparationList({ internalYes: !internalYes }, this.checkButtom()),
                pressPhoto: () => this.setState({ update: 3 }, this.setupCamera()),
                removeImg: this.removeSeparationImage
            }]
    } 

    setupCamera = () => {
        PhotoHelper(data => {
            this.setupImage(data);
        });
    } 

    checkButtom = () => {
        const { listCheckFilters, listCleanBox, listSeparation } = this.props;
            if ((listCheckFilters.imagesFilters.length > 0) || (listCheckFilters.message !== '')) {
                if ((listCleanBox.imagesCleanBox.length > 0) || (listCleanBox.message !== '')) {
                    if ((listSeparation.imagesSeparation.length > 0) || (listSeparation.message !== '')) {
                        this.setState({ click: true });
                    }
                }
            }
    }
    removeFilterImage = item => {
        this.setState({ img: item, modalVisible: true, update: 1 } );
    }
    removeBoxImage = item => {
        this.setState({ img: item, modalVisible: true, update: 2 } );
    }
    removeSeparationImage = item => {
        this.setState({ img: item, modalVisible: true, update: 3 } );
    }

    removeImgs = (img) => {
        switch(this.state.update) {
            case 1:
                this.rmFilter(img)
            break;
            case 2:
                this.rmBox(img)
             break;  
             case 3:
                this.rmSepara(img)
             break;
             default: 1; 
        }
    }

    rmFilter = (item) => {
        const { imageslocal, imagesFilters } = this.props.listCheckFilters;
        const index = imageslocal.findIndex(img => img === item);
        imageslocal.splice(index, 1);
        this.props.changefilterList({ imageslocal, imagesFilters: imageslocal });
        this.setState({ modalVisible: false });
    }
    rmBox = (item) => {
        const { imageslocal, imagesCleanBox } = this.props.listCleanBox;
        const index = imageslocal.findIndex(img => img === item);
        imageslocal.splice(index, 1);
        this.props.changeListBox({ imageslocal, imagesCleanBox: imageslocal });
        this.setState({ modalVisible: false });
    }
    rmSepara = (item) => {
        const { imageslocal } = this.props.listSeparation;
        const index = imageslocal.findIndex(img => img === item);
        imageslocal.splice(index, 1);
        this.props.changeSeparationList({ imageslocal, imagesSeparation: imageslocal });
        this.setState({ modalVisible: false });
    }

    setupImage = (img) => {
        switch(this.state.update) {
            case 1:
                const arr1 = [...this.props.listCheckFilters.imageslocal];
                const arr641 = [...this.props.listCheckFilters.imagesFilters];
                arr1.push(img);
                arr641.push(img);
                this.setState({ camera: false });
                this.props.changefilterList({ imageslocal: arr1, imagesFilters: arr641 });
            break;
            case 2:
                const arr2 = [...this.props.listCleanBox.imageslocal];
                const arr642 = [...this.props.listCleanBox.imagesCleanBox]
                arr2.push(img);
                arr642.push(img);
                this.setState({ camera: false });
                this.props.changeListBox({ imageslocal: arr2, imagesCleanBox: arr642 });
             break;
             case 3:
                const arr3 = [...this.props.listSeparation.imageslocal];
                const arr643 = [...this.props.listSeparation.imagesSeparation]
                arr3.push(img);
                arr643.push(img);
                this.setState({ camera: false });
                this.props.changeSeparationList({ imageslocal: arr3, imagesSeparation: arr643 });
                setTimeout(() => {
                    this.checkButtom();
                }, 100);
             break;
             default: 1; 
        }
    }

    saveData = () => {
        const { listCheckFilters, listCleanBox, listSeparation, inspectionDetail } = this.props;
        params = {
            filtros_diesel_vazamento: listCheckFilters.cleanYes,
            filtros_diesel_vazamento_obs: listCheckFilters.message,
            filtros_diesel_vazamento_imagens: [listCheckFilters.imagesFilters],
            limpeza_caixa_sepa_semana: listCleanBox.dryYes,
            limpeza_caixa_sepa_semana_obs: listCleanBox.message,
            limpeza_caixa_sepa_semana_imagens: [listCleanBox.imagesCleanBox],
            separacao_residuos: listSeparation.internalYes,
            separacao_residuos_obs: listSeparation.message,
            separacao_residuos_imagens: [listSeparation.imagesSeparation]
        }
        if (inspectionDetail === null) {
            if ((listCheckFilters.imagesFilters.length > 0) || (listCheckFilters.message !== '')) {
                if ((listCleanBox.imagesCleanBox.length > 0) || (listCleanBox.message !== '')) {
                    if ((listSeparation.imagesSeparation.length > 0) || (listSeparation.message !== '')) {
                        this.props.setInspection(params);
                        Actions.third_inspection();
                    }
                }
            }
        } else {
            Actions.third_inspection();
        }
        
    }

    goForward = () => {
        const { listCheckFilters, listCleanBox, listSeparation, inspectionDetail } = this.props;
        params = {
            filtros_diesel_vazamento: listCheckFilters.cleanYes,
            filtros_diesel_vazamento_obs: listCheckFilters.message,
            filtros_diesel_vazamento_imagens: [listCheckFilters.imagesFilters],
            limpeza_caixa_sepa_semana: listCleanBox.dryYes,
            limpeza_caixa_sepa_semana_obs: listCleanBox.message,
            limpeza_caixa_sepa_semana_imagens: [listCleanBox.imagesCleanBox],
            separacao_residuos: listSeparation.internalYes,
            separacao_residuos_obs: listSeparation.message,
            separacao_residuos_imagens: [listSeparation.imagesSeparation]
        }
    if (inspectionDetail === null) {
        if ((listCheckFilters.imagesFilters.length > 0) || (listCheckFilters.message !== '')) {
            if ((listCleanBox.imagesCleanBox.length > 0) || (listCleanBox.message !== '')) {
                if ((listSeparation.imagesSeparation.length > 0) || (listSeparation.message !== '')) {
                    this.props.setInspection(params);
                    return true;
                }
            }
        }
    } else {
        return true
    }
        
        return false;
    }

    setModal = () => {
        const { img } = this.state;
        return (
            <Modal
              visible={this.state.modalVisible}
              style={{ marginTop: 60 }}
              onRequestClose={() => {}}
            >
            <Form>
                <ImageBackground style={{ width: '100%', height: '100%' }} source={{ uri: img }}>
                <Form style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity
                        onPress={() => this.removeImgs(img)}
                        style={styles.capture}
                    >
                        <Text style={styles.txtBtn}> EXCLUIR IMAGEM </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.setState({ modalVisible: false })}
                        style={styles.capture}
                    >
                        <Text style={styles.txtBtn}> CANCELAR </Text>
                    </TouchableOpacity>
                </Form>
                </ImageBackground>
            </Form>
            </Modal>
        )
    }

    render() {
        const { inspectionDetail } = this.props;
        const header = inspectionDetail !== null ? inspectionDetail.nome : `Inspeção interna 0${this.props.inspectionQtd + 1}`; 
        const { click } = this.state;
        return (
            <Container>
                <NavbarAux title={header} />
                <Content>
                    {this.setModal()}
                    <InspectionItem
                        list={this.filterList()}
                        value={this.props.listCheckFilters.message}
                        editable={inspectionDetail !== null ? false : true}
                        autoCorrect={false}
                        onChangeText={(txt) => this.props.changefilterList({ message: txt }, this.checkButtom())}
                        placeholder='Digite...'
                        multiline
                        underlineColorAndroid='transparent'
                        textAlignVertical='top'
                        placeholderTextColor={colors.gray}
                        style={{ color: colors.gray }}
                        edit={inspectionDetail !== null ? true : false}
                    />
                    <InspectionItem 
                        list={this.listBox()}
                        value={this.props.listCleanBox.message}
                        editable={inspectionDetail !== null ? false : true}
                        autoCorrect={false}
                        onChangeText={(txt) => this.props.changeListBox({ message: txt }, this.checkButtom())}
                        placeholder='Digite...'
                        multiline
                        underlineColorAndroid='transparent'
                        textAlignVertical='top'
                        placeholderTextColor={colors.gray}
                        style={{ color: colors.gray }}
                        edit={inspectionDetail !== null ? true : false}
                    />
                    <InspectionItem
                        list={this.separationList()}
                        value={this.props.listSeparation.message}
                        editable={inspectionDetail !== null ? false : true}
                        autoCorrect={false}
                        onChangeText={(txt) => this.props.changeSeparationList({ message: txt }, this.checkButtom())}
                        placeholder='Digite...'
                        multiline
                        underlineColorAndroid='transparent'
                        textAlignVertical='top'
                        placeholderTextColor={colors.gray}
                        style={{ color: colors.gray }}
                        edit={inspectionDetail !== null ? true : false}
                    />
                </Content>
                <CountPage forward={this.goForward()} act2={{ backgroundColor: colors.green }} />
                <Button 
                    disabled={click ? false : true}
                    onPress={() => { this.saveData() }}
                    style={[styles.btn, !click ? styles.disable : {}]}>
                    <Label style={styles.btnTxt}>Salvar e ir para o próximo</Label>
                </Button>
            </Container>
        )
    }
}

const styles = {
    wrapper: {
        padding: 14
    },
    btn: {
        borderRadius: 20,
        width: '80%',
        height: 44,
        marginTop: 10,
        marginBottom: 20,
        backgroundColor: colors.green,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    btnTxt: {
        color: colors.white
    },
    disable: {
        opacity: 0.7
    },
    capture: {
        width: '40%',
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 6,
        paddingHorizontal: 8,
        margin: 15,
        alignItems: 'center',
        justifyContent: 'center'
      },
      txtBtn: {
        fontSize: 14,
        color: colors.blueMain
      },
} 

const mapStateToProps = state => ({
    listCheckFilters: state.inspection.listCheckFilters,
    listCleanBox: state.inspection.listCleanBox,
    listSeparation: state.inspection.listSeparation,
    inspectionQtd: state.inspection.inspectionQtd,
    inspectionDetail: state.inspection.inspectionDetail,
    data: state.client.clientFilds,
});

export default connect(mapStateToProps, {
    setInspection,
    changefilterList,
    changeListBox,
    changeSeparationList
})(ScoundInspection);
