import React from 'react';
import { Label, Form, Icon, Input } from 'native-base';
import { Image, ScrollView, TouchableOpacity } from 'react-native';
import { RadioButton } from '../../components/forms'
import { colors } from '../../utils';

const img = require('../../assets/icons/add-button.png');

class InspectionItem extends React.Component {

    back = () => {
        this.scroller.scrollTo({ x: 0 });
    }

    foward = () => {
        this.scroller.scrollTo({ x: 450 });
    }

    _inspectionItem = ({ list, edit, ...props }) => (
        <Form>
            {list.map((item, index) => (
                <Form key={index} style={styles.wrapper}>
                    <Form style={styles.body}>
                        <Label style={styles.txtTitle}>{item.title}</Label>
                        <RadioButton 
                            value={item.status} 
                            press={item.pressYesNo} 
                            label='Sim, tire até 8 fotos' 
                            edit={edit}
                        />
                        <Form style={{ display: item.status ? 'flex' : 'none' }}>
                            <Form style={styles.imgsArea}>
                                <Icon 
                                    name='ios-arrow-back'
                                    size={30}
                                    style={styles.icon} 
                                    onPress={() => this.back()}
                                />
                                <Form style={styles.scrollArea}>
                                    <ScrollView horizontal ref={(scroller) => {this.scroller = scroller}}>
                                        {item.images.map((im, i) => (                                         
                                            <TouchableOpacity disabled={edit} key={i} onPress={() => item.removeImg(im)}>
                                                <Image style={styles.img} source={im.imagem ? { uri: `https://engemax.leaderaplicativos.com.br/${im.imagem}` } : { uri: im }} />
                                            </TouchableOpacity>
                                        ))}
                                        <Form style={{ display: item.images.length <= 7 ? 'flex' : 'none' }}>
                                            <TouchableOpacity disabled={edit} onPress={item.pressPhoto} style={styles.imgPlus}>
                                                <Image style={styles.iconPlus} source={img} />
                                            </TouchableOpacity>
                                        </Form>
                                    </ScrollView>
                                </Form>
                                <Icon 
                                    name='ios-arrow-forward'
                                    size={30}
                                    style={styles.icon} 
                                    onPress={() => this.foward()}
                                />
                            </Form>
                        </Form>
                        <RadioButton 
                            value={!item.status} 
                            press={item.pressYesNo}
                            label='Não, por que?'
                            edit={edit}
                        />
                        <Form style={{ display: !item.status ? 'flex' : 'none' }}>
                            <Input {...props} />
                        </Form> 
                    </Form>
                </Form>
            ))}
        </Form>
    );
    render() {
        const { list, ...props } = this.props;
        return (
            <Form>
                {this._inspectionItem({ list, ...props })}
            </Form>
        )
    }
}

const styles = {
    wrapper: {
        borderBottomWidth: 1,
        borderBottomColor: colors.gray
    },
    body: {
        padding: 14
    },
    flexView: {
        display: 'flex'
    },
    imgPlus: {
        width: 50,
        height: 50,
        backgroundColor: colors.line,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgsArea: {
        flexDirection: 'row',
        alignItems: 'center',
        height: 50,
        flex: 1,
        marginTop: 10
    },
    scrollArea: {
        height: 50,
        flex: 1,
        marginRight: 8,
        marginLeft: 8
    },
    icon: {
        color: colors.black,
    },
    img: {
        width: 50,
        height: 50,
        marginRight: 8,
        alignItems: 'center',
        justifyContent: 'center'
    },
    iconPlus: {
        width: 30,
        height: 30
    },
    txtTitle: {
        fontSize: 15,
        color: colors.blueMain
    }
}

export default InspectionItem;
