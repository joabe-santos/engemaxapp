import React, { Component } from 'react';
import { Content, Container, Button, Label, Spinner } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Alert } from 'react-native';
import { connect } from 'react-redux';

import { NavbarAux } from '../../components/forms';
import { ItensTanks, ItensTanksCombustiveis, ItensTanksVolume } from '../../components/itensComponents/TanksComponent';
import { colors } from '../../utils';
import { getUserAcess } from '../../utils/localStore';
import baseApi from '../../services/baseApi';
import { getListTank } from './store/actions';

class EditTanks extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            filters: {
                selectedTipo: { value: '' },
                selectedVolume: { value: '' },
                selectedCombustivel: [],
            }
        };
    }

    componentWillMount() {
        const { item } = this.props;
        if (item) {
            if (item.combustivel !== null && item.combustivel2 !== null && item.combustivel3 !== null) {
                this.setState({
                    filters: {
                        ...this.state.filters,
                        selectedTipo: { value: item.tipo },
                        selectedVolume: { value: item.volume },
                        selectedCombustivel: [
                            { value: item.combustivel },
                            { value: item.combustivel2 },
                            { value: item.combustivel3 }
                        ]
                    }
                });
            } else if (item.combustivel !== null && item.combustivel2 !== null) {
                this.setState({
                    filters: {
                        ...this.state.filters,
                        selectedTipo: { value: item.tipo },
                        selectedVolume: { value: item.volume },
                        selectedCombustivel: [
                            { value: item.combustivel },
                            { value: item.combustivel2 }
                        ]
                    }
                });
            } else {
                this.setState({
                    filters: {
                        ...this.state.filters,
                        selectedTipo: { value: item.tipo },
                        selectedVolume: { value: item.volume },
                        selectedCombustivel: [
                            { value: item.combustivel }
                        ]
                    }
                });
            }
        }
        Actions.edit_tanks({ title: this.props.data.nome_fantasia });
    }

    listTanques = () => {
        return [
            { value: 'PLENO', label: 'Pleno' },
            { value: 'BIPARTIDO', label: 'Bipartido' },
            { value: 'TRIPARTIDO', label: 'Tripartido' }
        ]
    }

    listVolumes = () => {
        return [
            { value: '15000', label: '15000' },
            { value: '30000', label: '30000' },
            { value: '60000', label: '60000' }
        ]
    }

    listCombustiveis = () => {
        return [
            { value: 'GASOLINA_COMUM', label: 'Gasolina Comum' },
            { value: 'GASOLINA_ADITIVADA', label: 'Gasolina Aditivada' },
            { value: 'ETANOL', label: 'Etanol' },
            { value: 'DIESELS500', label: 'Diesel S500' },
            { value: 'DIESELS10', label: 'Diesel S10' },
        ]
    }

    changeTanque = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedTipo: {
                    value: valor.value
                },
                selectedCombustivel: []
            }
        })
    }

    changeVolume = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedVolume: {
                    value: valor.value
                }
            }
        })
    }

    changeCombustivel = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedCombustivel: valor
            }
        })
    }

    getAction = (token, params) => {
        const { item } = this.props;
        if (item) {
            return baseApi.tanks.editTanks(token, item.hashcod, params);
        }
        return baseApi.tanks.registerTanks(token, params);
    }

    checkSend = () => {
        const { selectedTipo, selectedCombustivel } = this.state.filters;
        if (selectedTipo.value === 'BIPARTIDO') {
            if (selectedCombustivel.length < 2) {
                alert('Você deve selecionar 2 opções.');
            } else {
                this.sendData();
            }
        } else if (selectedTipo.value === 'TRIPARTIDO') {
            if (selectedCombustivel.length < 3) {
                alert('Você deve selecionar 3 opções.');
            } else {
                this.sendData();
            }
        } else if (selectedTipo.value === 'PLENO') {
            if (selectedCombustivel.length < 1) {
                alert('Você deve selecionar 1 opção.');
            } else {
                this.sendData();
            }
        }
    }

    sendData = async () => {
        const formData = new FormData;
        this.setState({ loading: true })
        try {
            const acess = await getUserAcess();
            if (acess !== null) {
                formData.append('nome', 'Tanque');
                formData.append('tipo', this.state.filters.selectedTipo.value);
                formData.append('volume', this.state.filters.selectedVolume.value)
                const c = this.state.filters.selectedCombustivel;
                if (c.length === 1) {
                    formData.append('combustivel', c[0].value);
                    formData.append('combustivel2', '');
                    formData.append('combustivel3', '');
                }
                if (c.length === 2) {
                    formData.append('combustivel', c[0].value);
                    formData.append('combustivel2', c[1].value);
                    formData.append('combustivel3', '');
                }
                if (c.length === 3) {
                    formData.append('combustivel', c[0].value);
                    formData.append('combustivel2', c[1].value);
                    formData.append('combustivel3', c[2].value);
                }
                const response = await this.getAction(acess.token, formData);

                if (response.status === 200 || response.status === 201) {
                    this.setState({ loading: false })
                    Actions.popTo('tanks')
                    this.props.getListTank(acess.token)
                }
            }
        } catch (erro) {
            if (erro.response.status === 400) {
                Alert.alert('Erro!', 'Error ao recuperar dados.');
                this.setState({ loading: false });
            } else if (erro.response.status === 401) {
                Alert.alert('Erro!', 'Falha de autenticação faça o login novamente.');
                this.setState({ loading: false });
            }
            this.setState({ loading: false });
            console.log('erro catch::::', erro.response);
        }
    }

    getTankNumber = () => {
        if (this.props.item) {
            return this.props.item.index;
        }
        return this.props.tank;
    }


    render() {
        const { selectedTipo, selectedVolume, selectedCombustivel } = this.state.filters;
        return (
            <Container>
                <NavbarAux title='Tanques' />
                <Content style={{ margin: 10 }}>
                    <ItensTanks
                        title='Tipo'
                        data={this.listTanques()}
                        click={this.changeTanque}
                        selected={selectedTipo}
                        tanqueNumber={this.getTankNumber()}
                    />
                    <ItensTanksVolume
                        title='Volume total (Litros)'
                        data={this.listVolumes()}
                        click={this.changeVolume}
                        selected={selectedVolume} />
                    <ItensTanksCombustiveis
                        title='Combustível'
                        data={this.listCombustiveis()}
                        click={this.changeCombustivel}
                        selected={selectedCombustivel}
                        type={selectedTipo}
                    />
                </Content>
                <Button
                    onPress={() => { this.checkSend() }}
                    style={styles.btn}>
                    {this.state.loading ?
                        <Spinner color={colors.white} size='large' /> :
                        <Label style={styles.btnTxt}>Atualizar</Label>
                    }
                </Button>
            </Container>
        )
    }
}
const styles = {
    btn: {
        borderRadius: 20,
        width: 282,
        height: 44,
        marginTop: 110,
        marginBottom: 20,
        backgroundColor: colors.green,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    btnTxt: {
        color: colors.white
    },
}

const mapToProps = state => ({
    data: state.client.clientFilds,
})

export default connect(mapToProps, { getListTank })(EditTanks);
