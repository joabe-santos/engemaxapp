import baseApi from '../../../services/baseApi';

export const types = {
    get_list_actions: 'get_list_actions',
    get_list_loading: 'get_list_loading',
};

const getTanks = payload => ({
    type: types.get_list_actions,
    payload
});

export const getListTank = (token) => {
    return dispatch => {
        baseApi.tanks.getTanks(token)
            .then(response => {
                console.log(response)
                dispatch(getTanks(response.data))
                Loading(dispatch, false)
            }
            )
            .catch((erro) => {
                if (erro.response.status === 400)
                    Alert.alert('Erro!', 'Error ao recuperar dados.');
                Loading(dispatch, false);
            });
    }
}

const Loading = (dispatch, payload) => {
    dispatch({
        type: types.get_list_loading,
        payload
    })
}