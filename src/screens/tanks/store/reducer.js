import { types } from './actions';

const INITIAL_STATE = {
    loading: true,
    Listtank: [],
}

const reducer = (state = INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case types.get_list_actions:
            return { ...state, Listtank: payload };
        case types.get_list_loading:
            return { ...state, loading: payload }
        default:
            return state;
    }
}

export default reducer;