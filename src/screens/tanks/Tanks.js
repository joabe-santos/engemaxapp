import React from 'react';
import {
    Container,
    Form,
    Text,
} from 'native-base'
import {
    Image,
    TouchableOpacity,
    FlatList,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { colors } from '../../utils';
import { NavbarAux } from '../../components/forms';
import icons from '../../assets/icons';
import Spinner from '../../components/forms/Spinner';
import { getListTank } from './store/actions';
import { getUserAcess } from '../../utils/localStore';

class Tanks extends React.Component {
   
    async componentWillMount(){
        const acess = await getUserAcess();
        this.props.getListTank(acess.token);
        Actions.tanks({ title: this.props.data.nome_fantasia });
    }

    _renderItem = (item, index) => {
        let tipos = '';
        let combustivel = '';
        let combustivel2 = '';
        let combustivel3 = '';

        switch (item.tipo) {
            case 'PLENO':
                tipos = 'Pleno'
                break;
            case 'BIPARTIDO':
                tipos = 'Bipartido'
                break;
            case 'TRIPARTIDO':
                tipos = 'Tripartido'
                break;
        }
  
        switch (item.combustivel) {
            case 'GASOLINA_COMUM':
                combustivel = 'Gasolina Comum'
                break;
            case 'GASOLINA_ADITIVADA':
                combustivel = 'Gasolina Aditivada'
                break;
            case 'ETANOL':
                combustivel = 'Etanol'
                break;
            case 'DIESELS500':
                combustivel = 'Diesel S500'
                break;
            case 'DIESELS10':
                combustivel = 'Diesel S10'
        }
        switch (item.combustivel2) {
            case 'GASOLINA_COMUM':
                combustivel2 = 'Gasolina Comum'
                break;
            case 'GASOLINA_ADITIVADA':
                combustivel2 = 'Gasolina Aditivada'
                break;
            case 'ETANOL':
                combustivel2 = 'Etanol'
                break;
            case 'DIESELS500':
                combustivel2 = 'Diesel S500'
                break;
            case 'DIESELS10':
                combustivel2 = 'Diesel S10'
        }
        switch (item.combustivel3) {
            case 'GASOLINA_COMUM':
                combustivel3 = 'Gasolina Comum'
                break;
            case 'GASOLINA_ADITIVADA':
                combustivel3 = 'Gasolina Aditivada'
                break;
            case 'ETANOL':
                combustivel3= 'Etanol'
                break;
            case 'DIESELS500':
                combustivel3 = 'Diesel S500'
                break;
            case 'DIESELS10':
                combustivel3 = 'Diesel S10'
        }
        
        return(
        <Form>
            <TouchableOpacity onPress={() => { this._onItemPress(item, index) }}>
                <Form >
                    <Text style={styles.txtColor}>{item.nome} 0{index + 1}</Text>
                    <Text style={styles.txt}>{tipos}</Text>
                    <Text style={styles.txt}>{item.volume}</Text>
                    <Text style={styles.txt}>{combustivel}</Text>
                    <Text style={styles.txt}>{combustivel2}</Text>
                    <Text style={styles.txt}>{combustivel3}</Text>
                    <Form style={styles.borderLine}></Form>
                </Form>
            </TouchableOpacity>
        </Form>
        )
    }

    _onItemPress = (data, index) => {
        const item = { ...data, index: index }
        Actions.edit_tanks({ item });
    }

    _keyExtractor = (item) => item.hashcod;

    render() {
        if (this.props.loading) {
            return (
                <Spinner />
            )
        }
        return (
            <Container >
                <NavbarAux title='Tanques' />
                <FlatList
                    data={this.props.Listtank}
                    renderItem={({ item, index }) => this._renderItem(item, index)}
                    keyExtractor={this._keyExtractor}
                />
                <Form style={{ alignItems: 'center', marginBottom: 20 }}>
                    <TouchableOpacity onPress={() => Actions.edit_tanks({ tank: this.props.Listtank.length })}>
                        <Image style={{ height: 40, width: 40 }} source={icons['add_button']} />
                    </TouchableOpacity>
                </Form>
            </Container>
        )
    }
}

const styles = {

    txtColor: {
        marginLeft: 7,
        color: colors.blueMain,
        fontSize: 20
    },
    txt: {
        fontSize: 15,
        marginLeft: 7,
        color: colors.gray
    },
    borderLine: {
        height: 1,
        borderColor: colors.line,
        borderWidth: 0.5
    },

}

const mapStateToProps = state => (
    {
        loading: state.tankList.loading,
        Listtank: state.tankList.Listtank,
        data: state.client.clientFilds,
    }
)

export default connect(mapStateToProps,{ getListTank })(Tanks);
