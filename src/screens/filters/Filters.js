import React, { Component } from 'react';
import { Container, Content, Button, Label, Spinner } from 'native-base';
import { NavbarAux } from '../../components/forms';
import { ListDiesel, ListFiltros, ListSeparadoras, ListBox } from '../../components/itensComponents/FiltersComponent';
import { connect } from 'react-redux';
import { Alert } from 'react-native';

import { colors } from '../../utils';
import { Actions } from 'react-native-router-flux';
import baseApi from '../../services/baseApi';
import { getUserAcess } from '../../utils/localStore';

class Filters extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            loading2: false,
            filters: {
                selectedDiesil: { value: '' },
                selectedFiltros: { value: '' },
                selectedSeparadora: { value: '' },
                selectedSeparadoraQtd: { value: '' }
            }
        }
    }

    async componentWillMount() {
        this.setState({ loading2: true });
        try {
            const acess = await getUserAcess();
            if (acess !== null) {
                const response = await baseApi.filters.getFilters(acess.token);
                if (response.status === 200) {
                    this.setState({ loading2: false });
                    this.setState({
                        filters: {
                            ...this.state.filters,
                            selectedDiesil: { value: response.data.filtragem_diesel },
                            selectedSeparadora: { value: response.data.possui_caixa },
                            selectedSeparadoraQtd: { value: response.data.quantas_caixas },
                            selectedFiltros: { value: response.data.quantos_filtros },
                        }
                     });
                }
            }
        } catch(erro) {
            Alert .alert('Erro!','Falha ao carregar informações.');
            this.setState({ loading2: false });
        }
        Actions.filters({ title: this.props.data.nome_fantasia });
    }
        
    listOleo = () => {
        return [{ value: 'SIM', label: 'Sim' }, { value: 'NAO', label: 'Não' }]
    }
    QFilters = () => {
        return [
            { value: '1', label: '1' },
            { value: '2', label: '2' },
            { value: '3', label: '3' },
            { value: '4', label: '4' },
            { value: '5OUACIMA', label: 'Acima de 5' }
        ]
    }
    ListBox = () => {
        return [{ value: 'SIM', label: 'Sim' }, { value: 'NAO', label: 'Não' }]
    }
    QSeparadoras = () => {
        return [
            { value: '1', label: '1' },
            { value: '2', label: '2' },
            { value: '3', label: '3' },
            { value: '4', label: '4' },
            { value: '5OUACIMA', label: 'Acima de 5' }
        ]
    }

    onChange = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedDiesil: { ...valor }
            }
        })
    }
    onChange1 = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedFiltros: { ...valor }
            }
        })
    }
    onChange2 = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedSeparadora: { ...valor }
            }
        })
    }
    onChange3 = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedSeparadoraQtd: { ...valor }
            }
        })
    }

    sendData = async () => {
        this.setState({ loading: true });
        try {
            const acess = await getUserAcess();
            if (acess !== null) {
                const params = {
                    filtragem_diesel: this.state.filters.selectedDiesil.value,
                    quantos_filtros: this.state.filters.selectedFiltros.value,
                    possui_caixa: this.state.filters.selectedSeparadora.value,
                    quantas_caixas: this.state.filters.selectedSeparadoraQtd.value
                };
                const response = await baseApi.filters.updateFilters(acess.token, params);
                if (response.status === 200) {
                    console.log('ENVIAR', response)
                    Actions.inspencao();
                    this.setState({ loading: false });
                }
            }
        } catch(erro) {
           Alert.alert('Erro!','Falha ao atualizar as informações, tente novamente.');
            this.setState({ loading: false });
        }
    }

    render() {
        if (this.state.loading2) {
            return (
                <Spinner style={styles.spinner} size='large' color={colors.blueMain} />
            )
        }
        const { selectedDiesil, selectedFiltros, selectedSeparadora, selectedSeparadoraQtd } = this.state.filters;
        return (
            <Container>
                <NavbarAux title='Filtros' />
                <Content>
                    <ListDiesel
                        title='Possui filtragem de diesel?' 
                        editable={selectedDiesil.value !== null ? true : false}
                        click={this.onChange} 
                        data={this.listOleo()} 
                        selected={selectedDiesil} 
                    />
                    <ListFiltros 
                        title='Quantos filtros?'
                        click={this.onChange1} 
                        data={this.QFilters()} 
                        selected={selectedDiesil.value === 'NAO' ? selectedFiltros.value=null : selectedFiltros}
                        edit={selectedDiesil.value === 'NAO' ? true : false}
                    />
                    <ListBox
                        title='Possui caixa separadora' 
                        editable={selectedSeparadora.value !== null ? false : true}
                        click={this.onChange2} 
                        data={this.ListBox()} 
                        selected={selectedSeparadora}
                    />
                    <ListSeparadoras 
                        title='Quantos?'
                        click={this.onChange3} 
                        data={this.QSeparadoras()} 
                        selected={selectedSeparadora.value === 'NAO' ? selectedSeparadoraQtd.value=null : selectedSeparadoraQtd}
                        edit={selectedSeparadora.value === 'NAO' ? true : false}
                    />
                </Content>
                <Button
                    onPress={() => { this.sendData() }}
                    style={styles.btn}>
                    {this.state.loading ?
                        <Spinner color={colors.white} size='large' /> :
                        <Label style={styles.btnTxt}>Atualizar</Label>
                    }
                </Button>
            </Container>
        )
    }
}

const styles = {
    btn: {
        borderRadius: 20,
        width: 282,
        height: 44,
        marginTop: 110,
        marginBottom: 20,
        backgroundColor: colors.green,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    btnTxt: {
        color: colors.white
    },
    spinner: {
        position: 'absolute',
        marginTop: '40%',
        alignSelf: 'center'
    }
}

const mapToProps = state => ({
    data: state.client.clientFilds,
})

export default connect(mapToProps, null)(Filters);

