import React, { Component } from 'react';
import { Alert } from 'react-native';
import { Container, Content, Label, Button, Spinner } from 'native-base';
import { ListCombustivel, ListConveniencia, ListTrocaOleo, ListVeiculos, ListTercerizada } from '../../components/itensComponents/ServicesComponent';
import { connect } from 'react-redux';

import { NavbarAux } from '../../components/forms/';
import { colors } from '../../utils'
import { Actions } from 'react-native-router-flux';
import baseApi from '../../services/baseApi';
import { getUserAcess } from '../../utils/localStore';

class Services extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            loading2: false,
            filters:{
                selectedGas: { label: '', value: '' },
                selectedConveniencia: { label: '', value: '' },
                selectedOleo: { label: '', value: '' },
                selectedLavagem: { label: '', value: '' },
                selectedTercerizada: { label: '', value: '' },
            }
        };
    }

    async componentWillMount() {
        this.setState({ loading2: true });
        try {
            const acess = await getUserAcess();
            if (acess !== null) {
                const services = await baseApi.services.getServices(acess.token);
                if (services.status === 200) {
                    this.setState({
                        filters: {
                            ...this.state.filters,
                            selectedGas: { value: services.data.abastecimento_combustivel },
                            selectedConveniencia: { value: services.data.loja_conveniencia },
                            selectedOleo: { value: services.data.troca_oleo },
                            selectedLavagem: { value: services.data.lavagem },
                            selectedTercerizada: { value: services.data.lavagem_tipo }
                        }
                     });
                     this.setState({ loading2: false });
                }
            }
        } catch(erro) {
            Alert.alert('Error!','Erro ao recuperar dados');
            this.setState({ loading2: false });
        }
        Actions.services({ title: this.props.data.nome_fantasia });
    }

    listCombustivel = () => {
        return [{ value: 'SIM', label: 'Sim' }, { value: 'NAO', label: 'Não' }]
    }
    listConveniencia = () => {
        return [{ value: 'SIM', label: 'Sim' }, { value: 'NAO', label: 'Não' }]
    }
    listTrocaOleo = () => {
        return [{ value: 'SIM', label: 'Sim' }, { value: 'NAO', label: 'Não' }]
    }
    listLavagem = () => {
        return [{ value: 'SIM', label: 'Sim' }, { value: 'NAO', label: 'Não' }]
    }
    listIntTer = () => {
        return [{ value: 'INTERNA', label: 'Interna' }, { value: 'TERCERIZADA', label: 'Tercerizada' }]
    }

    changeCombustivel = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedGas: { ...valor }
            }
        })
    }
    changeConveniencia = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedConveniencia: { ...valor }
            }
        })
    }
    changeTrocaOleo = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedOleo: { ...valor }
            }
        })
    }
    changeLavagem = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedLavagem: { ...valor }
            }
        })
    }
    changeIntTer = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedTercerizada: { ...valor }
            }
        })
    }

    sendData = async () => {
        this.setState({ loading: true });
        try {
            const acess = await getUserAcess();
            if (acess !== null) {
                const params = {
                    abastecimento_combustivel: this.state.filters.selectedGas.value,
                    loja_conveniencia: this.state.filters.selectedConveniencia.value,
                    troca_oleo: this.state.filters.selectedOleo.value,
                    lavagem: this.state.filters.selectedLavagem.value,
                    lavagem_tipo: this.state.filters.selectedTercerizada.value
                };
                const response = await baseApi.services.updateServices(acess.token, params);
                if (response.status === 200) {                 
                    Actions.inspencao();
                    this.setState({ loading: false });
                } 
            }
        } catch(erro) {
            if (erro.response.status === 400) {
                Alert.alert('Erro!', 'Valor enviado inválido.');
                this.setState({ loading: false });
            } else if (erro.response.status === 401) {
                Alert.alert('Erro!', 'Falha de autenticação faça o login novamente.');
                this.setState({ loading: false });
            }
            this.setState({ loading: false });
            console.log('erro catch::::', erro.response);
        }
    }

    render() {
        if (this.state.loading2) {
            return (
                <Spinner style={styles.spinner} size='large' color={colors.blueMain} />
            )
        }
        const { selectedGas, selectedConveniencia, selectedOleo, selectedLavagem, selectedTercerizada } = this.state.filters;
        return (
            <Container>
                <NavbarAux title='Serviços' />
                <Content>
                    <ListCombustivel
                        title='Abastecimento de combustivíel?'
                        data={this.listCombustivel()}
                        click={this.changeCombustivel}
                        selected={selectedGas}
                    />
                    <ListConveniencia
                        title='Loja de Conveniência?'
                        data={this.listConveniencia()}
                        click={this.changeConveniencia}
                        selected={selectedConveniencia}
                    />
                    <ListTrocaOleo
                        title='Realiza troca ou complementação de oléo?'
                        data={this.listTrocaOleo()}
                        click={this.changeTrocaOleo}
                        selected={selectedOleo}
                    />
                    <ListVeiculos
                        title='Realiza lavagem de veículos?'
                        data={this.listLavagem()}
                        click={this.changeLavagem}
                        selected={selectedLavagem}
                    />
                    <ListTercerizada
                        title='Se sim, ela é interna ou tercerizada?'
                        data={this.listIntTer()}
                        click={this.changeIntTer}
                        selected={selectedLavagem.value === 'NAO' ? selectedTercerizada.value=null : selectedTercerizada}
                        edit={selectedLavagem.value === 'NAO' ? true : false}
                    />
                </Content>
                <Button
                    onPress={() => this.sendData()}
                    style={styles.btn}>
                    {this.state.loading ? 
                        <Spinner color={colors.white} size='large' /> :
                        <Label style={styles.btnTxt}>Atualizar</Label>
                    }
                </Button>
            </Container>
        );
    }
}

const styles = {
    btn: {
        borderRadius: 20,
        width: 282,
        height: 44,
        marginTop: 10,
        marginBottom: 20,
        backgroundColor: colors.green,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    btnTxt: {
        color: colors.white
    },
    spinner: {
        position: 'absolute',
        marginTop: '40%',
        alignSelf: 'center'
    }
}

const mapToProps = state => ({
    data: state.client.clientFilds,
})

export default connect(mapToProps, null)(Services);
