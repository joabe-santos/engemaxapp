import React, { Component } from 'react';
import { Alert } from 'react-native';
import {
  Form, 
  Button, 
  Input, 
  Item as FormItem, 
  Label,
  Content,
  Spinner
} from 'native-base';

import { ImageBackground } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { imagens } from '../../utils';
import { styles } from './Styles';
import { colors } from '../../utils/';
import apiService from '../../services/baseApi';

class RecoverPassScreen extends Component {
  state = {
    email: '',
    loading: false
  };

  handleRecoverPass = async () => {
    const { email } = this.state;
    const acess = { email };
    this.setState({ loading: true });

    if (email !== '') {
      try {
        const response = await apiService.auth.recoverPass(acess);
        if (response.status === 201) {
          this.setState({ loading: false, email: '' });
          Alert.alert('Sucesso!', 'E-mail de recuperaçao enviado.');
          Actions.login();
        }
      } catch (error) {
        this.setState({ loading: false });
        Alert.alert('Erro!', 'Problema no envio.');
      }
    } else {
      this.setState({ loading: false });
      Alert.alert('Erro!', 'Campo e-mail obrigatório');
    }
  };

  render() {
    return(
      <ImageBackground 
        style={styles.backgroundImage}
        source={imagens['img_back_login']}
      >
        <Content contentContainerStyle={Styles.contentContainer}> 
          <FormItem rounded style={styles.input}>
            <Input 
              value={this.state.email}
              style={styles.inputTxt}
              placeholderTextColor={colors.white}
              placeholder='E-mail'
              onChangeText={value => { this.setState({ email: value }) }}
            />
          </FormItem>
          <Form padder style={{ paddingTop:30 }}>
            <Button onPress={() => {
              this.handleRecoverPass();
            }} 
              style={styles.button}>
              {this.state.loading ?
                <Spinner color={colors.white} /> :
                <Label style={{ color: colors.white }}>Solicitar nova senha</Label>
              }
            </Button>
          </Form>    
        </Content>
      </ImageBackground>
    )
  }
}

const Styles = {
  contentContainer:{
    justifyContent: 'center', 
    alignItems: 'center',
    paddingTop: 220,
  },
};

export default RecoverPassScreen;

