import { colors } from '../../utils';

export const styles = {
    contentContainer:{
        justifyContent: 'center', 
        alignItems: 'center',
        paddingTop: 160,
    },
    backgroundImage: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover'
    },
    input:{
        margin:5, 
        width:282,
        height:44,
        backgroundColor:'rgba(155,155,155,0.90)',
        borderRadius: 20,
        
    },
    button:{
        borderRadius:20,
        width:282,
        height:44, 
        backgroundColor:'#54893B',
        justifyContent:'center',
        alignItems:'center'
    },
    txtButton:{
        color:'#fff', 
        paddingTop:18
    },
    inputTxt: {
        marginLeft: 10, 
        color: colors.white
    }

}