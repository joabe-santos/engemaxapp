import { types } from './actions';

const INITIAL_STATE = {
    email: '',
    password: '',
    erroLogin: '',
    loading_login: false,
}

const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case types.email_login:
            return { ...state, email: action.payload };
        case types.pass_login:
            return { ...state, password: action.payload };
        case types.login_user_sucess:
            return { ...state, ...INITIAL_STATE }
        case types.loading_login:
            return { ...state, loading_login: true }
        case types.login_user_error:
            return { ...state, erroLogin: action.payload, loading_login: false }
        default:
            return state;
    }
};

export default reducer;

