// acess to api services
import apiService from '../../../services/baseApi'
import { Alert } from 'react-native';
import { setUserAcess } from '../../../utils/localStore';
import { Actions } from 'react-native-router-flux';

export const types = {
    email_login: 'email_login',
    pass_login: 'pass_login',
    auth_login_ok: 'login/auth_login_ok',
    login_user_sucess: 'login_user_sucess',
    login_user_error: 'login_user_error',
    loading_login: 'loading_login',
};

export const changeEmail = (texto) => {
    return {
        type: types.email_login,
        payload: texto
    }
}
export const changePass = (texto) => {
    return {
        type: types.pass_login,
        payload: texto
    }
}

export const handleLogin = (email, password) => {
    const acess = { email, password }
    return dispatch => {
        dispatch({ type: types.loading_login })
        apiService.auth.login(acess)
            .then(response => {
                if (response.status === 200) {
                    console.log(response)
                    setUserAcess({email, password, token: response.data.token})
                    LoginUserSucess(dispatch)
                }
            })
            .catch(erro => {
                loginUserError(erro, dispatch)
        });
    }
}

const LoginUserSucess = (dispatch) => {
    dispatch({ type: types.login_user_sucess })
    Actions.inspencao();
}

const loginUserError = (erro, dispatch) => {
    menssagemError = "";
    if (erro.response.status === 400) {
        if (erro.response.data.non_field_errors) {
            menssagemError = (erro.response.data.non_field_errors[0]);
        } else if (erro.response.data.password) {
            menssagemError = (erro.response.data.password[0]);
        } else if (erro.response.data.email) {
            menssagemError = (erro.response.data.email[0]);
        }
    }
    dispatch({ type: types.login_user_error, payload: Alert.alert('Erro!',menssagemError) })
}
