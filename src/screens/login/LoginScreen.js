import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ImageBackground, StatusBar } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {
  Button,
  Input,
  Item as FormItem,
  Label,
  Form,
  Content,
  Spinner
} from 'native-base';

import { imagens, colors } from '../../utils';
import { styles } from './Styles';
import { changeEmail, changePass, handleLogin } from './store/actions';


class LoginScreen extends Component {

  _setupLogin() {
    const { email, password } = this.props;
    this.props.handleLogin(email, password)
  }

  render() {
    return (
      <ImageBackground style={styles.backgroundImage} source={imagens['img_back_login']}>
        <StatusBar transparent barStyle="light-content" />
        <Content contentContainerStyle={styles.contentContainer}>
          <FormItem rounded style={styles.input}>
            <Input
              value={this.props.email}
              style={styles.inputTxt}
              placeholderTextColor={colors.white}
              placeholder='Email'
              onChangeText={texto => this.props.changeEmail(texto)}
            />
          </FormItem>
          <FormItem rounded style={styles.input}  >
            <Input
              value={this.props.password}
              style={styles.inputTxt}
              placeholderTextColor={colors.white}
              secureTextEntry
              placeholder='Senha'
              onChangeText={texto => this.props.changePass(texto)}
            />
          </FormItem>
          <Form style={{ paddingTop: 30 }}>
            <Button onPress={() => {
              this._setupLogin();
            }}
              style={styles.button}
            >
              {this.props.loading_login ?
                <Spinner color={colors.white} /> :
                <Label style={{ color: colors.white }}>Entrar</Label>
              }
            </Button>
          </Form>
          <Form>
            <Button
              onPress={() => { Actions.recovery(); }}
              hasText
              transparent
            >
              <Label style={styles.txtButton}>Esqueceu a Senha?</Label>
            </Button>
          </Form>
        </Content>
      </ImageBackground>
    )
  }
}

const mapStateToProps = state => (
  {
    email: state.auth.email,
    password: state.auth.password,
    loading_login: state.auth.loading_login,
  }
)

export default connect(mapStateToProps, { changeEmail, changePass, handleLogin })(LoginScreen);
