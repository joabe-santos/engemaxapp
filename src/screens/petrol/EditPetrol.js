import React, { Component } from 'react';
import { Content, Container, Button, Label, Spinner } from 'native-base';
import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';

import { NavbarAux } from '../../components/forms';
import { ItensBombas, ItensCombustiveis } from '../../components/itensComponents/PetrolComponent';
import { colors } from '../../utils';
import { getUserAcess } from '../../utils/localStore';
import baseApi from '../../services/baseApi';
import { getListPetrol } from './store/actions';

class EditPetrol extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            filters: {
                selectedTipo: { value: '' },
                selectedCombustivel: [],
            }
        };
    }

    componentWillMount() {
        const { item } = this.props;
        /*if (item) {
            this.setState({
                filters: {
                    ...this.state.filters,
                    selectedTipo: { value: item.tipo },
                    selectedCombustivel: [
                        { value: item.combustivel === null ? item.combustivel = '' : item.combustivel },
                        { value: item.combustivel2 === null ? item.combustivel2 = '' : item.combustivel2 },
                        { value: item.combustivel3 === null ? item.combustivel3 = '' : item.combustivel3 }
                    ]
                }
            });
        } */
        if (item) {
            if (item.combustivel !== null && item.combustivel2 !== null && item.combustivel3 !== null) {
                this.setState({
                    filters: {
                        ...this.state.filters,
                        selectedTipo: { value: item.tipo },
                        selectedVolume: { value: item.volume },
                        selectedCombustivel: [
                            { value: item.combustivel },
                            { value: item.combustivel2 },
                            { value: item.combustivel3 }
                        ]
                    }
                });
            } else if (item.combustivel !== null && item.combustivel2 !== null) {
                this.setState({
                    filters: {
                        ...this.state.filters,
                        selectedTipo: { value: item.tipo },
                        selectedVolume: { value: item.volume },
                        selectedCombustivel: [
                            { value: item.combustivel },
                            { value: item.combustivel2 }
                        ]
                    }
                });
            } else {
                this.setState({
                    filters: {
                        ...this.state.filters,
                        selectedTipo: { value: item.tipo },
                        selectedVolume: { value: item.volume },
                        selectedCombustivel: [
                            { value: item.combustivel }
                        ]
                    }
                });
            }
        }
        Actions.edit_petrol({ title: this.props.data.nome_fantasia })
    }

    listBombas = () => {
        return [
            { value: "SIMPLES", label: "Simples" },
            { value: "DUPLA", label: "Dupla" },
            { value: "TRIPLA", label: "Tripla" }
        ]
    }

    listCombustiveis = () => {
        return [
            { value: 'GASOLINA_COMUM', label: 'Gasolina Comum' },
            { value: 'GASOLINA_ADITIVADA', label: 'Gasolina Aditivada' },
            { value: 'ETANOL', label: 'Etanol' },
            { value: 'DIESELS500', label: 'Diesel S500' },
            { value: 'DIESELS10', label: 'Diesel S10' },
        ]
    }

    changeTipo = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedTipo: {
                    value: valor.value
                },
                selectedCombustivel: []
            }
        })
    }

    changeCombustivel = valor => {
        this.setState({
            filters: {
                ...this.state.filters,
                selectedCombustivel: valor
            }
        })
    }

    getAction = (token, params) => {
        const { item } = this.props;
        if (item) {
            return baseApi.petrol.editPetrol(token, item.hashcod, params);
        }
        return baseApi.petrol.registerPetrol(token, params);
    }

    /*checkSend = () => {
        const { selectedTipo, selectedCombustivel } = this.state.filters;
        if (selectedTipo.value === 'DUPLA') {
            if (selectedCombustivel.length < 2) {
                alert('Você deve selecionar 2 opções.');
            } else {
                this.sendData();
            }
        } else if (selectedTipo.value === 'TRIPLA') {
            if (selectedCombustivel.length < 3) {
                alert('Você deve selecionar 3 opções.');
            } else {
                this.sendData();
            }
        } else if (selectedTipo.value === 'SIMPLES') {
            if (selectedCombustivel.length < 1) {
                alert('Você deve selecionar 1 opção.');
            } else {
                this.sendData();
            }
        }
    } */

    sendData = async () => {
        const formData = new FormData();
        this.setState({ loading: true })
        try {
            const acess = await getUserAcess();
            if (acess !== null) {
                formData.append('nome', 'Bomba');
                formData.append('tipo', this.state.filters.selectedTipo.value);
                const c = this.state.filters.selectedCombustivel;
                if(c.length === 1) {
                    formData.append('combustivel', c[0].value);
                    formData.append('combustivel2', '' );
                    formData.append('combustivel3', '');
                }
                if(c.length === 2) {
                    formData.append('combustivel', c[0].value);
                    formData.append('combustivel2', c[1].value);
                    formData.append('combustivel3', '');
                }
                if(c.length === 3) {
                    formData.append('combustivel', c[0].value);
                    formData.append('combustivel2', c[1].value);
                    formData.append('combustivel3', c[2].value);    
                }
                const response = await this.getAction(acess.token, formData);
                if (response.status === 200 || response.status === 201) {
                    Actions.petrol();
                    this.setState({ loading: false })
                    this.props.getListPetrol(acess.token)
                }
            }
        } catch (erro) {
            if (erro.response.status === 400) {
                Alert.alert('Erro!', 'Valor enviado inválido.');
                this.setState({ loading: false });
            } else if (erro.response.status === 401) {
                Alert.alert('Erro!', 'Falha de autenticação faça o login novamente.');
                this.setState({ loading: false });
            }
            this.setState({ loading: false });

        }
    }

    getBombNumber = () => {
        if (this.props.item) {
            return this.props.item.index;
        }
        return this.props.bomba;
    }

    render() {
        const { selectedTipo, selectedCombustivel } = this.state.filters;

        return (
            <Container>
                <NavbarAux title='Bombas' />
                <Content style={{ margin: 10 }}>
                    <ItensBombas
                        title='Tipo'
                        data={this.listBombas()}
                        click={this.changeTipo}
                        selected={selectedTipo}
                        bombaNunber={this.getBombNumber()}
                    />
                    <ItensCombustiveis
                        title='Combustível'
                        data={this.listCombustiveis()}
                        click={this.changeCombustivel}
                        selected={selectedCombustivel}
                        type={selectedTipo}
                    />
                </Content>
                <Button
                    onPress={() => { this.sendData() }}
                    style={styles.btn}>
                    {this.state.loading ?
                        <Spinner color={colors.white} size='large' /> :
                        <Label style={styles.btnTxt}>Atualizar</Label>
                    }
                </Button>
            </Container>
        )
    }
}

const styles = {
    btn: {
        borderRadius: 20,
        width: 282,
        height: 44,
        marginTop: 110,
        marginBottom: 20,
        backgroundColor: colors.green,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    btnTxt: {
        color: colors.white
    },
}

const mapToProps = state => ({
    data: state.client.clientFilds,
})

export default connect(mapToProps, { getListPetrol })(EditPetrol);
