import baseApi from '../../../services/baseApi';

export const types = {
    get_list_petrol_actions: 'get_list_petrol_actions',
    get_list_petrol_loading: 'get_list_petrol_loading',
};

const getpetroList = payload => ({
    type: types.get_list_petrol_actions,
    payload
});

export const getListPetrol = (token) => {
    return dispatch => {
        baseApi.petrol.getPetrol(token)
            .then(response => {
                dispatch(getpetroList(response.data))
                Loading(dispatch, false)
                }
            )
            .catch((erro) => {
                if (erro.response.status === 400)
                    Alert.alert('Erro!', 'Error ao recuperar dados.');
                Loading(dispatch, false);
            });
    }
}

const Loading = (dispatch, payload) => {
    dispatch({
        type: types.get_list_petrol_loading,
        payload
    })
}