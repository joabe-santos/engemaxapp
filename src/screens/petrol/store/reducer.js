import { types } from './actions';

const INITIAL_STATE = {
    loading: true,
    ListPetrol: [],
}

const reducer = (state = INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case types.get_list_petrol_actions:
            return { ...state, ListPetrol: payload };
        case types.get_list_petrol_loading:
            return { ...state, loading: payload }
        default:
            return state;
    }
}

export default reducer;