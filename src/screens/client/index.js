import React, { Component } from 'react';
import { Image, View, TouchableOpacity } from 'react-native';
import { Container, Content, Button, Label, Spinner, Form } from 'native-base';
import { Actions } from 'react-native-router-flux';
import VMasker from 'vanilla-masker';
import { connect } from 'react-redux';

import { changeClientFild, changeCameraState, registerUserAction, searchCepAction } from './store/actions';
import { InputText, NavbarAux } from '../../components/forms';
import { colors } from '../../utils';
import { imageValidate, PhotoHelper } from '../../utils/PhotoHelper';
import { getUserAcess } from '../../utils/localStore';


class UserDataScreen extends Component {

    componentDidMount() {
        Actions.user_data_screen({ title: this.props.data.nome_fantasia });
        this.email()
    }
    state = {
        cep: '',
    }

    handleHeader = () => {
        if (this.props.cameraStatus.img === null) {
            return (
                <View style={styles.header}>
                    <TouchableOpacity onPress={() => this.setupCamera()}>
                        <Image style={styles.img} source={imageValidate(this.props.filds.imagem)} />
                    </TouchableOpacity>
                </View>
            )
        }
        return (
            <View style={styles.header}>
                <TouchableOpacity onPress={() => this.setupCamera()}>
                    <Image style={styles.img} source={imageValidate(this.props.cameraStatus.img)} />
                </TouchableOpacity>
            </View>
        )
    }

    setupCamera = () => {
        PhotoHelper(data => {
            this.setupImage(data);
        });
    } 

    _cepSearch = () => {
        if (this.state.cep !== '') {
            this.props.searchCepAction(this.state.cep);
        }
    }
    email = async () => {
        const user = await getUserAcess();
        this.props.changeClientFild({ email: user.email })
    }

    setupImage = (img) => {
        this.props.changeCameraState({ img });
        this.props.changeClientFild({ imagem: img });
    }

    sendData = async () => {
        const user = await getUserAcess();
        const userData = this.props.filds;
        this.props.registerUserAction(user.token, userData);
    }

    render() {
        const { cnpj, razao_social, nome_fantasia, imagem, email, telefone, cep, rua, bairro, cidade, estado } = this.props.filds;
        return (
            <Container>
                <NavbarAux title='Dados Cadastrais' />
                <Content style={styles.wrapper}>
                    <Form>
                        {this.handleHeader()}
                        <InputText
                            title='CNPJ'
                            value={cnpj}
                            editable
                            autoCorrect={false}
                            onChangeText={value => this.props.changeClientFild({ cnpj: VMasker.toPattern(value, '99.999.999/9999-99') })}
                            multiline={false}
                            underlineColorAndroid='transparent'
                            keyboardType="numeric"
                        />
                        <InputText
                            value={razao_social}
                            title='Razão Social'
                            editable
                            autoCorrect={false}
                            onChangeText={value => this.props.changeClientFild({ razao_social: value })}
                            multiline
                            underlineColorAndroid='transparent'
                        />
                        <InputText
                            value={nome_fantasia}
                            title='Nome Fantasia'
                            editable
                            autoCorrect={false}
                            onChangeText={value => this.props.changeClientFild({ nome_fantasia: value })}
                            multiline={false}
                            underlineColorAndroid='transparent'
                        />
                        <InputText
                            value={email}
                            title='E-mail'
                            edit={email !== null ? true : false}
                            autoCorrect={false}
                            onChangeText={value => this.props.changeClientFild({ email: value })}
                            multiline={false}
                            underlineColorAndroid='transparent'

                        />
                        <InputText
                            value={telefone}
                            title='Telefone'
                            editable
                            autoCorrect={false}
                            onChangeText={value => this.props.changeClientFild({ telefone: VMasker.toPattern(value, '(99) 99999-9999') })}
                            multiline={false}
                            underlineColorAndroid='transparent'
                            keyboardType="numeric"
                        />
                        <InputText
                            value={cep}
                            title='CEP'
                            editable
                            autoCorrect={false}
                            onChangeText={value => {
                                this.props.changeClientFild({ cep: VMasker.toPattern(value, '99999-999') })
                                this.setState({ cep: value })
                            }
                            }
                            multiline={false}
                            underlineColorAndroid='transparent'
                            keyboardType="numeric"
                            onBlur={this._cepSearch}

                        />
                        <InputText
                            value={rua}
                            title='Rua'
                            editable
                            autoCorrect={false}
                            onChangeText={value => this.props.changeClientFild({ rua: value })}
                            multiline={false}
                            underlineColorAndroid='transparent'
                        />
                        <InputText
                            value={bairro}
                            title='Bairro'
                            editable
                            autoCorrect={false}
                            onChangeText={value => this.props.changeClientFild({ bairro: value })}
                            multiline={false}
                            underlineColorAndroid='transparent'
                        />
                        <InputText
                            value={cidade}
                            title='Cidade'
                            editable
                            autoCorrect={false}
                            onChangeText={value => this.props.changeClientFild({ cidade: value })}
                            multiline={false}
                            underlineColorAndroid='transparent'
                        />
                        <InputText
                            value={estado}
                            title='Estado'
                            editable
                            autoCorrect={false}
                            onChangeText={value => this.props.changeClientFild({ estado: value })}
                            multiline={false}
                            underlineColorAndroid='transparent'
                        />
                        <Button
                            onPress={() => { this.sendData() }}
                            style={styles.btn}>
                            {this.props.loading ?
                                <Spinner size='large' color={colors.white} />
                                :
                                <Label style={styles.btnTxt}>Atualizar</Label>
                            }
                        </Button>
                    </Form>
                </Content>
            </Container>
        );
    }
}

const styles = {
    header: {
        alignItems: 'center'
    },
    wrapper: {
        paddingRight: 10
    },
    btnArea: {
        position: 'absolute',
        bottom: 20
    },
    btn: {
        borderRadius: 20,
        width: 282,
        height: 44,
        marginTop: 20,
        marginBottom: 20,
        backgroundColor: colors.green,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center'
    },
    btnTxt: {
        color: colors.white
    },
    img: {
        padding: 20,
        width: 100,
        height: 100,
        marginBottom: 20,
        marginTop: 20
    }
};

stateToProps = state => ({
    filds: state.client.clientFilds,
    cameraStatus: state.client.cameraImg,
    loading: state.client.loading,
    data: state.client.clientFilds,
});

export default connect(stateToProps, {
    changeClientFild,
    changeCameraState,
    registerUserAction,
    searchCepAction
})(UserDataScreen);
