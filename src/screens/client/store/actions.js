import baseApi from '../../../services/baseApi';
import { setUser } from '../../../utils/localStore';
import axios from 'axios';
import { Actions } from 'react-native-router-flux';

export const types = {
    client_change_fild: 'client/client_change_fild',
    client_camera_state: 'client/client_camera_state',
    client_loading: 'cliente/cliente_loading'
};

export const changeClientFild = (payload) => ({
    type: types.client_change_fild,
    payload
});

export const changeCameraState = (payload) => ({
    type: types.client_camera_state,
    payload
});

export const registerUserAction = (token, data) => {
    const formData = new FormData();
    formData.append('imagem', { uri: data.imagem, type: 'image/jpeg', name: 'imagem.jpg' });
    formData.append('cnpj', data.cnpj);
    formData.append('razao_social', data.razao_social);
    formData.append('nome_fantasia', data.nome_fantasia);
    formData.append('email', data.email);
    formData.append('telefone', data.telefone);
    formData.append('cep', data.cep);
    formData.append('rua', data.rua);
    formData.append('bairro', data.bairro);
    formData.append('cidade', data.cidade);
    formData.append('estado', data.estado);
    console.log("clientes,", formData);
     return dispatch => {
        setupLoading(dispatch, true)
        baseApi.register.registerUser(token, formData)
            .then(response => {
                changeClientFild(dispatch, response.data);
                setUser(response.data);
                setupLoading(dispatch, false);
                Actions.inspencao({ title: response.data.nome_fantasia });
            })
            .catch(erro => {
                if(erro.response.status === 400) {
                    if (erro.response.data.imagem) {
                        alert('Erro ao salvar as informações.', erro.response.data.imagem[0]);
                    }
                } else {
                    alert('Erro ao salvar as informações.');
                }
                setupLoading(dispatch, false);
                console.log('erro salvar dados::', erro.response);
            })
    }
}

const setupLoading = (dispatch, payload) => {
    dispatch({
        type: types.client_loading,
        payload
    })
}

export const searchCepAction = (cep) => {
    return dispatch => {
        axios.get(`https://viacep.com.br/ws/${cep}/json/`, {
            timeout: 10000,
            headers: { 'Content-Type': 'application/json' }
        })
            .then(response => {
                if (response.status === 200) {
                    const params = {
                        cep: response.data.cep,
                        rua: response.data.logradouro,
                        bairro: response.data.bairro,
                        cidade: response.data.localidade,
                        estado: response.data.uf,
                    }
                    updateAdress(dispatch, params);
                }
            })
            .catch(error => {
                alert('Erro ao buscar a localização.');
            })
    }
}

const updateAdress = (dispatch, payload) => {
    dispatch({
        type: types.client_change_fild,
        payload
    })
} 
