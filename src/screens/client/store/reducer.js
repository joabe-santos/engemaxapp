import { types } from './actions';

const DEFAULT_VALUES = {
    clientFilds: {
        imagem: null,
        cnpj: '',
        razao_social: '',
        nome_fantasia: '',
        email: '',
        telefone: '',
        cep: '',
        rua: '',
        bairro: '',
        cidade: '',
        estado: '',
    },
    cameraImg: {
        img: null,
        camera: false,
    },
    loading: false
}

const reducer = (state = DEFAULT_VALUES, { type, payload }) => {
    switch (type) {
        case types.client_change_fild:
            return {
                ...state,
                clientFilds: {
                    ...state.clientFilds,
                    ...payload
                },
            };
        case types.client_camera_state:
            return {
                ...state,
                cameraImg: {
                    ...state.cameraImg,
                    ...payload
                }
            }
        case types.client_loading:
            return { 
                ...state, 
                loading: payload 
            }
        default:
            return state
    }
}

export default reducer;
