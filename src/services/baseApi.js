import axios from 'axios';

const BASE_URL = 'https://engemax.leaderaplicativos.com.br';
const AUTH = '/api/api-token-auth/';
const RECOVER_PASS = '/api/v1/usuario/solicitar-senha/';
const REGISTER_USER = '/api/v1/usuario/posto/';
const REGISTER_PETROL = '/api/v1/bombas/';
const SERVICES = '/api/v1/usuario/servicos/';
const FILTERS = '/api/v1/usuario/filtros/';
const TANKS = '/api/v1/tanques/';
const INSPECOES_INTERNAS = '/api/v1/inspecoes-internas/';
const INSPECTIONS_LIST = '/api/v1/inspecoes-internas/listagem/cadastrados/';
const EDIT_PASS = '/api/v1/usuario/alterar-senha/';

const endpoints = {
  requestLogin: () => `${AUTH}`,
  requestNewPass: () => `${RECOVER_PASS}`,
  registerUserEndPoint: () => `${REGISTER_USER}`,
  registerPetrolEndPoint: () => `${REGISTER_PETROL}`,
  editPetrolEndPoint: hash => `${REGISTER_PETROL}${hash}/`,
  deletePetrolEndPoint: hash => `${REGISTER_PETROL}${hash}/`,
  registerServices: () => `${SERVICES}`,
  registerFilters: () => `${FILTERS}`,
  registerTanksEndPoint: () => `${TANKS}`,
  editTanksEndPoint: hash => `${TANKS}${hash}/`,
  registerInspecoesEndpoint: () => `${INSPECOES_INTERNAS}`,
  getInspecoesEndpointDetail: hash => `${INSPECOES_INTERNAS}${hash}/`,
  getInspecoesEndpoint: () => `${INSPECTIONS_LIST}`,
  editPassEndpoint: () => `${EDIT_PASS}`,
};

const getHeaders = (token) => {
  const userToken = { Authorization: `JWT ${token}` }
  return userToken;
}

const leaderApi = axios.create({
  baseURL: BASE_URL,
  timeout: 60000,
  headers: { 'Content-Type': 'application/json' }
});

const apiService = {
  auth: {
    login: (params) =>
      leaderApi.post(endpoints.requestLogin(), params, {
        headers: { 'Content-Type': 'application/json' }
      }),
    recoverPass: (params) =>
      leaderApi.post(endpoints.requestNewPass(), params, {
        headers: { 'Content-Type': 'application/json' }
      })
  },
  register: {
    registerUser: (token, params) =>
      leaderApi.put(endpoints.registerUserEndPoint(), params, {
        headers: getHeaders(token),
      }),
    getUser: (token) =>
      leaderApi.get(endpoints.registerUserEndPoint(), {
        headers: getHeaders(token),
      }),
  },
  services: {
    getServices: (token) =>
      leaderApi.get(endpoints.registerServices(), {
        headers: getHeaders(token),
      }),
    updateServices: (token, params) =>
      leaderApi.put(endpoints.registerServices(), params, {
        headers: getHeaders(token),
      }),
  },
  petrol: {
    registerPetrol: (token, params) =>
      leaderApi.post(endpoints.registerPetrolEndPoint(), params, {
        headers: getHeaders(token),
      }),
    editPetrol: (token, hash, params) =>
      leaderApi.put(endpoints.editPetrolEndPoint(hash), params, {
        headers: getHeaders(token),
      }),
    getPetrol: (token) =>
      leaderApi.get(endpoints.registerPetrolEndPoint(), {
        headers: getHeaders(token),
      }),
    deletePetrol: (token, hash) =>
      leaderApi.delete(endpoints.deletePetrolEndPoint(hash), {
        headers: getHeaders(token),
      }),
  },
  tanks: {
    registerTanks: (token, params) =>
      leaderApi.post(endpoints.registerTanksEndPoint(), params, {
        headers: getHeaders(token)
      }),
    editTanks: (token, hash, params) =>
      leaderApi.put(endpoints.editTanksEndPoint(hash), params, {
        headers: getHeaders(token)
      }),
    deleteTanks: (token, hash, params) =>
      leaderApi.delete(endpoints.editTanksEndPoint(hash), params, {
        headers: getHeaders(token)
      }),
    getTanks: (token) =>
      leaderApi.get(endpoints.registerTanksEndPoint(), {
        headers: getHeaders(token)
      }),
  },
  filters: {
    getFilters: (token) =>
      leaderApi.get(endpoints.registerFilters(), {
        headers: getHeaders(token),
      }),
    updateFilters: (token, params) =>
      leaderApi.put(endpoints.registerFilters(), params, {
        headers: getHeaders(token),
      }),
  },
  inspecoes: {
    registerInspecoes: (token, formDada) => 
      leaderApi.post(endpoints.registerInspecoesEndpoint(), formDada, {
       headers: getHeaders(token),
     }),
    getInspecoes: (token) => 
      leaderApi.get(endpoints.getInspecoesEndpoint(), {
        headers: getHeaders(token),
    }),
    getInspectionDetail: (token, hash) =>
      leaderApi.get(endpoints.getInspecoesEndpointDetail(hash), {
        headers: getHeaders(token),
    }),
  },
  edit: {
    editPass: (token, params) => 
    leaderApi.post(endpoints.editPassEndpoint(), params,{
      headers: getHeaders(token)
    }),
  }
}
export default apiService;